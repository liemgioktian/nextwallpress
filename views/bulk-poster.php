<div class="wrap">
  <h1>Bulk Poster</h1><div class="notice notice-info"><p>Make Google love your images with Image Sitemap Pro <a href="https://udinra.com/downloads/udinra-image-sitemap-pro">Know More</a> | <a href="?udinra_image_admin_ignore=0">Hide Notice</a></p></div>
  <div class="bootstrap-wrapper" id="mwp-bulk-poster" style="margin-top: 15px;">
    <div style="margin-bottom: 20px;"></div>
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h2 class="panel-title">Documentation</h2>
          </div>
          <div class="panel-body">
            <p>Read the documentation for more informations.</p>
            <a class="btn btn-info btn-block" href="http://insfires.com/docs/magic-wallpress#bulk-poster" target="_blank">Open Documentation</a>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="panel panel-info">
          <div class="panel-body">
            <form action="" method="post" class="form-horizontal form-bordered">

              <div class="form-group">
                <label class="control-label col-md-3" for="total">How Many Posts</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="total" type="number" class="form-control" required="">
                    <span class="input-group-addon" id="basic-addon2">Posts</span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" for="campaign">Campaigns</label>
                <div class="col-md-9">
                  <select class="form-control" id="campaign" multiple="multiple" required="true">
                    <option value="1">Sofa Bed</option><option value="2">Sleeper Sofa</option><option value="3">Sectional Sofa</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" for="total">Post Interval</label>
                <div class="col-md-9">
                  <div class="input-group">
                    <input id="total" type="number" class="form-control" required="">
                    <span class="input-group-addon" id="basic-addon2">SettingsHours</span>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3" for="post-date-select-option">Publish Date</label>
                <div class="col-md-9">
                  <select class="form-control">
                    <option value="0">Now</option>
                    <option value="1">Custom Date</option>
                  </select>
                </div>
              </div>
              <div class="form-group" style="display: none;">
                <label class="control-label col-md-3" for="post-1-keyword-date">Date</label>
                <div class="col-md-9">
                  <input type="text" class="form-control mwp-date hasDatepicker" id="dp1497191219994">
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-md-12">
                  <button type="submit" class="btn btn-primary pull-right">Run</button>
                </div>
              </div>
            </form>
          </div>
        </div>

        
      </div>
    </div>
  </div>
</div>