<div class="wrap" id="mwp-manage-post" style="">
  <h1>Post Management</h1><div class="notice notice-info"><p>Make Google love your images with Image Sitemap Pro <a href="https://udinra.com/downloads/udinra-image-sitemap-pro">Know More</a> | <a href="?udinra_image_admin_ignore=0">Hide Notice</a></p></div>
  <div class="bootstrap-wrapper" style="margin-top: 25px;">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h2 class="panel-title">Showing 1 to 20 of 92 posts.</h2>
          </div>
          <div class="panel-body">
            <form class="form-inline">
              <div class="pull-right">
                
                <button type="button" class="btn btn-default">Next <i class="fa fa-angle-double-right"></i></button>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">Show</div>
                  <input type="number" class="form-control">
                  <div class="input-group-addon">Keywords</div>
                </div>
              </div>
            </form>
            <form action="" method="post" class="form-horizontal form-bordered" style="padding-top:20px;">
              <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th>
                      <a href="#"><i class="fa fa-sort pull-right"></i> Post Title</a>
                      
                    </th>
                    <th>
                      <a href="#"><i class="fa fa-sort pull-right"></i> Keyword</a>
                      
                    </th>
                    <th>
                      <a href="#"><i class="fa fa-sort pull-right"></i> Campaign</a>
                      
                    </th>
                    <th>
                      <a href="#">
                        <i class="fa fa-sort-amount-desc pull-right"></i> Publish Date                      </a>
                      
                    </th>
                    <th>
                      <a href="#"><i class="fa fa-sort pull-right"></i> Images</a>
                      
                    </th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><input type="checkbox" value="0"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2205&amp;action=edit">Inspirational Unique Sofa</a>
                    </td>
                    <td>unique sofa</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=3">Sectional Sofa</a>
                    </td>
                    <td>2017-06-09 12:57:23</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2205">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2205&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="1"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2124&amp;action=edit">Lovely Great Sofa</a>
                    </td>
                    <td>great sofa</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=2">Sleeper Sofa</a>
                    </td>
                    <td>2017-06-09 07:40:52</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2124">80</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2124&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="2"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2101&amp;action=edit">Awesome Sectional Sofa Pull Out Bed</a>
                    </td>
                    <td>Sectional Sofa Pull Out Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-06 11:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2101">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2101&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="3"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2079&amp;action=edit">Best of Tufted Sofa Bed</a>
                    </td>
                    <td>Tufted Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-06 08:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2079">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2079&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="4"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2057&amp;action=edit">Inspirational Ashley Furniture Sofa Beds</a>
                    </td>
                    <td>Ashley Furniture Sofa Beds</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-06 05:00:02</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2057">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2057&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="5"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2035&amp;action=edit">Unique Sofa Beds For Small Spaces</a>
                    </td>
                    <td>Sofa Beds For Small Spaces</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-06 01:00:02</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2035">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2035&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="6"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2013&amp;action=edit">New Memory Foam Sofa Bed</a>
                    </td>
                    <td>Memory Foam Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 22:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2013">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2013&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="7"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=2010&amp;action=edit">Beautiful Sofa Bed Craigslist</a>
                    </td>
                    <td>Sofa Bed Craigslist</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 21:00:02</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=2010">2</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=2010&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="8"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1988&amp;action=edit">Awesome Twin Size Sofa Bed</a>
                    </td>
                    <td>Twin Size Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 17:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1988">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1988&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="9"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1978&amp;action=edit">Best of Lazy Boy Sofa Bed</a>
                    </td>
                    <td>Lazy Boy Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 16:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1978">9</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1978&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="10"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1956&amp;action=edit">Elegant Convertible Sofa Bed With Storage</a>
                    </td>
                    <td>Convertible Sofa Bed With Storage</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 13:00:02</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1956">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1956&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="11"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1934&amp;action=edit">New Mattress For Sofa Bed</a>
                    </td>
                    <td>Mattress For Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 11:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1934">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1934&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="12"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1912&amp;action=edit">Inspirational Cheap Futon Sofa Bed</a>
                    </td>
                    <td>Cheap Futon Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 08:00:02</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1912">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1912&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="13"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1901&amp;action=edit">Awesome Rent A Center Sofa Beds</a>
                    </td>
                    <td>Rent A Center Sofa Beds</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 07:00:05</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1901">10</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1901&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="14"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1879&amp;action=edit">Best of Cheap Sofa Beds For Sale</a>
                    </td>
                    <td>Cheap Sofa Beds For Sale</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-05 03:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1879">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1879&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="15"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1857&amp;action=edit">Elegant Balkarp Sofa Bed</a>
                    </td>
                    <td>Balkarp Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-04 23:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1857">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1857&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="16"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1835&amp;action=edit">Elegant L Shaped Sofa Bed</a>
                    </td>
                    <td>L Shaped Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-04 20:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1835">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1835&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="17"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1813&amp;action=edit">Lovely Sofa Bed Sheets</a>
                    </td>
                    <td>Sofa Bed Sheets</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-04 17:00:02</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1813">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1813&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="18"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1791&amp;action=edit">Awesome Sears Sofa Bed</a>
                    </td>
                    <td>Sears Sofa Bed</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-04 13:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1791">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1791&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr><tr>
                    <td><input type="checkbox" value="19"></td>
                    <td>
                      <a title="Edit from post editor" target="_blank" href="post.php?post=1769&amp;action=edit">Beautiful Sofa Bed Mattress Replacement</a>
                    </td>
                    <td>Sofa Bed Mattress Replacement</td>
                    <td>
                      <a title="Manage campaign" target="_blank" href="admin.php?page=magic-wallpress-campaign&amp;manage_campaign=1">Sofa Bed</a>
                    </td>
                    <td>2017-06-04 10:00:01</td>
                    <td>
                      <a title="View or Add Image Attachments" target="_blank" href="?page=magic-wallpress-manage-post&amp;post=1769">20</a>
                    </td>
                    <td class="text-center">
                      <a class="btn btn-xs btn-success" title="View Post" target="_blank" href="/?p=1769&amp;preview=true"><i class="fa fa-eye"></i></a>
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateKeyword" title="Update Content"><i class="fa fa-check-square-o"></i></button>

                      <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteKeyword" title="Delete"><i class="fa fa-times"></i></button>
                    </td>
                  </tr>
                </tbody>
                
              </table>
            </form>
            <form class="form-inline">
              <div class="pull-right">
                
                <button type="button" class="btn btn-default">Next <i class="fa fa-angle-double-right"></i></button>
              </div>
              <div class="form-group">
                <select class="form-control" disabled="">
                  <option value="0">---</option>
                  <option value="bulkUpdates">Update</option>
                  <option value="bulkDeletes">Delete</option>
                </select>
              </div>
              <button type="button" class="btn btn-primary" data-toggle="modal" disabled="" data-target="#0">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div id="updateKeyword" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Update Post () Content</h4>
            </div>
            <div class="modal-body">
              <div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="updating-update-attachments">Also Update Attachments Contents</label>
                  <div class="col-md-9">
                    <select class="form-control" id="updating-update-attachments">
                      <option value="0">No</option>
                      <option value="1">Yes</option>
                    </select>
                  </div>
                </div>

                <div class="form-group" style="display: none;">
                  <label class="control-label col-md-3" for="updating-update-captions">Also Update Images Caption</label>
                  <div class="col-md-9">
                    <select class="form-control" id="updating-update-captions">
                      <option value="0">No</option>
                      <option value="1">Yes</option>
                    </select>
                  </div>
                </div>
              </div>
              
              
            </div>
              
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Update Content</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="bulkUpdates" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Bulk Update Posts Content</h4>
            </div>
            <div class="modal-body">
              <div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="updating-update-attachments">Also Update Attachments Contents</label>
                  <div class="col-md-9">
                    <select class="form-control" id="updating-update-attachments">
                      <option value="0">No</option>
                      <option value="1">Yes</option>
                    </select>
                  </div>
                </div>
                
                <div class="form-group" style="display: none;">
                  <label class="control-label col-md-3" for="updating-update-captions">Also Update Images Caption</label>
                  <div class="col-md-9">
                    <select class="form-control" id="updating-update-captions">
                      <option value="0">No</option>
                      <option value="1">Yes</option>
                    </select>
                  </div>
                </div>
              </div>
              
              
            </div>
            
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Update Content</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="deleteKeyword" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Delete Keyword ()</h4>
            </div>
            <div class="modal-body">
              <div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="deleting-delete_post">Also Delete Post</label>
                  <div class="col-md-9">
                    <select class="form-control" id="deleting-delete_post">
                      <option value="0">No</option>
                      <option value="1">Yes</option>
                    </select>
                  </div>
                </div>
                
              </div>
              
              
            </div>
              
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Delete</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="bulkDeletes" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Bulk Delete Keywords</h4>
            </div>
            <div class="modal-body">
              <div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="deleting-delete_post">Also Delete Post</label>
                  <div class="col-md-9">
                    <select class="form-control" id="deleting-delete_post">
                      <option value="0">No</option>
                      <option value="1">Yes</option>
                    </select>
                  </div>
                </div>
                
              </div>
              
              
            </div>
              
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Delete</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>