<div class="wrap">
  <h1>Settings</h1><div class="notice notice-info"><p>Make Google love your images with Image Sitemap Pro <a href="https://udinra.com/downloads/udinra-image-sitemap-pro">Know More</a> | <a href="?udinra_image_admin_ignore=0">Hide Notice</a></p></div>
  <div class="bootstrap-wrapper" id="mwp-settings" style="margin-top: 15px;">
    <div class="row">
      <div class="col-md-4">
        <div class="panel panel-info">
          <div class="panel-heading">
            <h2 class="panel-title">Documentation</h2>
          </div>
          <div class="panel-body">
            <p>Read the documentation for more informations.</p>
            <a class="btn btn-info btn-block" href="http://insfires.com/docs/magic-wallpress#setting" target="_blank">Open Documentation</a>
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="panel panel-info">
          <div class="panel-body">
            <form action="" method="post" class="form-horizontal form-bordered">
              <button type="submit" class="btn btn-primary pull-right">
                 Update             </button>
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#default">Default</a></li>
                <li><a data-toggle="tab" href="#keyword">Keyword Poster</a></li>
                <li><a data-toggle="tab" href="#image">Image Poster</a></li>
                <li><a data-toggle="tab" href="#advanced">Advanced</a></li>
                <li><a data-toggle="tab" href="#importExport">Import &amp; Export</a></li>
              </ul>
              <div class="tab-content" style="margin-top: 20px">
                

                <div id="default" class="tab-pane fade in active">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="post_title_format">Post Title Format</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="post_title_format">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="attachment_title_format">Attachment Title Format</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="attachment_title_format">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="image_alt_format">Image Alt Format</label>
                    <div class="col-md-9">
                      <input type="text" class="form-control" id="image_alt_format">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="image_size">Image Size</label>
                    <div class="col-md-9">
                      <select id="image_size" class="form-control">
                        <option value="any">Any</option>
                        <option value="small">Small</option>
                        <option value="medium">Medium</option>
                        <option value="large">Large</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="post_template">Post Template</label>
                    <div class="col-md-9">
                      <select class="form-control" id="post_template" multiple="multiple">
                        <option value="3">Atigan Post 20</option><option value="4">Atigan Post 30</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="attachment_template">Attachment Template</label>
                    <div class="col-md-9">
                      <select class="form-control" id="attachment_template" multiple="multiple">
                        <option value="1">Atigan Attachment 3</option><option value="5">Atigan Attachment 4</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="image_caption_template">Image Caption Template</label>
                    <div class="col-md-9">
                      <select class="form-control" id="image_caption_template" multiple="multiple">
                        <option value="2">Atigan Caption 3</option><option value="6">Atigan Caption 4</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="categories">Post Categories</label>
                    <div class="col-md-9">
                      <select class="form-control" id="categories" multiple="multiple">
                        <option value="3">Living Room</option><option value="11">Sectional Sofa</option><option value="1">Sleeper Sofa</option><option value="2">Sofa Bed</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="authors">Authors</label>
                    <div class="col-md-9">
                      <select class="form-control" id="authors" multiple="multiple">
                        <option value="1">el</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div id="keyword" class="tab-pane fade">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="keep_draft">Post Status</label>
                    <div class="col-md-9">
                      <select class="form-control" id="keep_draft">
                        <option value="no">Publish</option>
                        <option value="yes">Draft</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="attachment_per_post">Minimum Images</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="attachment_per_post" type="number" class="form-control">
                        <span class="input-group-addon" id="basic-addon2">Images per Post</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="generate_post_content">Generate Post Content</label>
                    <div class="col-md-9">
                      <select class="form-control" id="generate_post_content">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                      <span class="help-block"><strong>Generate post content from selected template</strong></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="generate_attachment_content">Generate Attachment Content</label>
                    <div class="col-md-9">
                      <select class="form-control" id="generate_attachment_content">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                      <span class="help-block"><strong>Generate attachment content from selected template</strong></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="generate-tag">Generate Post Tags</label>
                    <div class="col-md-9">
                      <select class="form-control" id="generate-tag">
                        <option value="none">No</option>
                        <option value="suggest">Google Suggest</option>
                        <option value="space">Split Word by Space</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="tags">How Many Tags</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="tags" type="number" class="form-control">
                        <span class="input-group-addon" id="basic-addon2">Tags</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="image" class="tab-pane fade">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="imageQuality">Image Quality</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="imageQuality" type="number" class="form-control" required="">
                        <span class="input-group-addon" id="basic-addon2">% (Percent)</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="resize_method">Resize Method</label>
                    <div class="col-md-9">
                      <select class="form-control" id="resize_method">
                        <option value="disable">Disable</option>
                        <option value="exact">Exact (Pixel)</option>
                        <option value="exactInPercent">Exact (Percentage)</option>
                        <option value="portrait">Portrait (Pixel)</option>
                        <option value="portraitInPercent">Portrait (Percentage)</option>
                        <option value="landscape">Landscape (Pixel)</option>
                        <option value="landscapeInPercent">Landscape (Percentage)</option>
                        <option value="auto">Auto (Pixel)</option>
                        <option value="autoInPercent">Auto (Percentage)</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-md-3" for="widthInPercent">Image Width</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="widthInPercent" type="number" class="form-control" required="">
                        <span class="input-group-addon" id="basic-addon2">% (Percent)</span>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-md-3" for="heightInPercent">Image Height</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="heightInPercent" type="number" class="form-control" required="">
                        <span class="input-group-addon" id="basic-addon2">% (Percent)</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="flip_method">Flip Method</label>
                    <div class="col-md-9">
                      <select class="form-control" id="flip_method">
                        <option value="disable">Disable</option>
                        <option value="horizontal">Horizontal</option>
                        <option value="vertical">Vertical</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="opacity">Opacity</label>
                    <div class="col-md-9">
                      <select class="form-control" id="opacity">
                        <option value="disable">Disable</option>
                        <option value="enable">Enable</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="opcityPercentage">Opacity Percentage</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="opcityPercentage" type="number" class="form-control" required="">
                        <span class="input-group-addon" id="basic-addon2">% (Percent)</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="rotate">Rotate</label>
                    <div class="col-md-9">
                      <select class="form-control" id="rotate">
                        <option value="disable">Disable</option>
                        <option value="enable">Enable</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-md-3" for="meta_data">Reset Image Meta Data</label>
                    <div class="col-md-9">
                      <select class="form-control" id="meta_data">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div id="advanced" class="tab-pane fade">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="use_proxy">Use Proxy?</label>
                    <div class="col-md-9">
                      <select class="form-control" id="use_proxy">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label col-md-3" for="remove_words">Removable Words</label>
                    <div class="col-md-9">
                      <span class="help-block"><strong>Separate each word by line. Use triple bracket if word started or ended with space.</strong></span>
                      <textarea rows="8" class="form-control" id="remove_words" placeholder="All of words listed here will be removed from image title.
Separated by new line"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="bad_words">Bad Words</label>
                    <div class="col-md-9">
                      <span class="help-block"><strong>Separate each word by line</strong></span>
                      <textarea rows="8" class="form-control" id="bad_words" placeholder="Place your bad words list here.
Separated by new line"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="bad_domains">Bad Domains</label>
                    <div class="col-md-9">
                      <span class="help-block"><strong>Separate each domain by line</strong></span>
                      <textarea rows="8" class="form-control" id="bad_domains" placeholder="Place your bad domains list here.
Separated by new line"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="youtube_apis">Youtube API's</label>
                    <div class="col-md-9">
                      <span class="help-block"><strong>Separate each API key by line</strong></span>
                      <textarea rows="8" class="form-control" id="youtube_apis" placeholder="Place your youtube api's list here.
Separated by new line"></textarea>
                    </div>
                  </div>
                </div>

                <div id="importExport" class="tab-pane fade">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="exportText">Import Settings</label>
                    <div class="col-md-9">
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importSetting"><i class="fa fa-arrow-circle-o-left"></i> Import from code</button>
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#importDefault"><i class="fa fa-undo"></i> Restore default</button>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="exportText">Backup Settings</label>
                    <div class="col-md-9">
                      <span class="help-block"><strong>Text below is the backup code for current settings</strong></span>
                      <textarea rows="8" class="form-control" id="exportText">YTozMzp7czoxNzoicG9zdF90aXRsZV9mb3JtYXQiO3M6MTAwOiJbJUF3ZXNvbWV8THV4dXJ5fEVsZWdhbnR8QmVzdCBvZnxCZWF1dGlmdWx8RnJlc2h8SW5zcGlyYXRpb25hbHxMb3ZlbHl8VW5pcXVlfE5ldyVdIHt7a2V5d29yZHx0aXRsZX19IjtzOjIzOiJhdHRhY2htZW50X3RpdGxlX2Zvcm1hdCI7czoxNTY6Int7aW1hZ2UudGl0bGV8dGl0bGV9fSBbJWZvcnx3aXRoIHJlZ2FyZCB0b3xyZWdhcmRpbmd8cGVydGFpbmluZyB0b3xpbnRlbmRlZCBmb3J8aW58d2l0aGlufGluc2lkZXx0aHJvdWdob3V0fHdpdGglXSBbJXt7a2V5d29yZHx0aXRsZX19fHt7cG9zdC5wb3N0X3RpdGxlfX0lXSI7czoxNjoiaW1hZ2VfYWx0X2Zvcm1hdCI7czoyNToie3thdHRhY2htZW50LnBvc3RfdGl0bGV9fSI7czoxMDoiaW1hZ2Vfc2l6ZSI7czo1OiJsYXJnZSI7czoxMzoicG9zdF90ZW1wbGF0ZSI7YToxOntpOjA7czoxOiI5Ijt9czoxOToiYXR0YWNobWVudF90ZW1wbGF0ZSI7YToxOntpOjA7czoxOiI0Ijt9czoyMjoiaW1hZ2VfY2FwdGlvbl90ZW1wbGF0ZSI7YToxOntpOjA7czoxOiI1Ijt9czoxMDoiY2F0ZWdvcmllcyI7YToxOntpOjA7czozOiIxNzgiO31zOjc6ImF1dGhvcnMiO2E6MTp7aTowO3M6MToiMSI7fXM6MTA6ImtlZXBfZHJhZnQiO3M6Mjoibm8iO3M6MTk6ImF0dGFjaG1lbnRfcGVyX3Bvc3QiO3M6MjoiMjAiO3M6MjE6ImdlbmVyYXRlX3Bvc3RfY29udGVudCI7czozOiJ5ZXMiO3M6Mjc6ImdlbmVyYXRlX2F0dGFjaG1lbnRfY29udGVudCI7czozOiJ5ZXMiO3M6MTI6ImdlbmVyYXRlX3RhZyI7czo3OiJzdWdnZXN0IjtzOjQ6InRhZ3MiO3M6MToiMiI7czoxMjoiaW1hZ2VRdWFsaXR5IjtzOjM6IjEwMCI7czoxMzoicmVzaXplX21ldGhvZCI7czoxMzoiYXV0b0luUGVyY2VudCI7czo1OiJ3aWR0aCI7czo0OiIxMDI0IjtzOjY6ImhlaWdodCI7czo0OiIxMDI0IjtzOjE0OiJ3aWR0aEluUGVyY2VudCI7czoyOiI5NSI7czoxNToiaGVpZ2h0SW5QZXJjZW50IjtzOjI6Ijk1IjtzOjExOiJmbGlwX21ldGhvZCI7czoxMDoiaG9yaXpvbnRhbCI7czo3OiJvcGFjaXR5IjtzOjY6ImVuYWJsZSI7czoxNjoib3BjaXR5UGVyY2VudGFnZSI7czozOiIxMDAiO3M6Njoicm90YXRlIjtzOjc6ImRpc2FibGUiO3M6MTM6InJvdGF0ZURlZ3JlZXMiO3M6MjoiLTIiO3M6OToibWV0YV9kYXRhIjtzOjM6InllcyI7czo5OiJ1c2VfcHJveHkiO3M6Mjoibm8iO3M6OToiYmFkX3dvcmRzIjthOjQ6e2k6MDtzOjM6InNleCI7aToxO3M6NDoicG9ybiI7aToyO3M6NDoiYW5hbCI7aTozO3M6NDoibnVkZSI7fXM6MTI6InJlbW92ZV93b3JkcyI7YToxNTp7aTowO3M6MTA6Int7eyBieSB9fX0iO2k6MTtzOjQ6Ii5jb20iO2k6MjtzOjQ6Ii5uZXQiO2k6MztzOjQ6Ii5vcmciO2k6NDtzOjU6Ii5pbmZvIjtpOjU7czozOiIudXMiO2k6NjtzOjM6Ii5tZSI7aTo3O3M6NDoiLmpwZyI7aTo4O3M6NDoiLnBuZyI7aTo5O3M6NToiLmpwZWciO2k6MTA7czo0OiJzYWxlIjtpOjExO3M6NToic3RvcmUiO2k6MTI7czo2OiJvbmxpbmUiO2k6MTM7czo2OiJhbWF6b24iO2k6MTQ7czozOiIuLi4iO31zOjExOiJiYWRfZG9tYWlucyI7YTozOntpOjA7czoxMDoiYW1hem9uLmNvbSI7aToxO3M6MTA6Imdvb2dsZS5jb20iO2k6MjtzOjY6ImFkcy5pZCI7fXM6MTI6InlvdXR1YmVfYXBpcyI7YToyOntpOjA7czozOToiQUl6YVN5QVg0RTd4b2VEOVFTX1VBODc0dkNfbDVTUnp5cFl4QURZIjtpOjE7czozOToiQUl6YVN5QlkzSXVqd0o0dHV2dmxDcUJabnc4TzZfOEMwLUpUME93Ijt9czo3OiJwcm94aWVzIjthOjE6e2k6MDtzOjA6IiI7fX0=</textarea>
                    </div>
                  </div>
                </div>

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div id="importSetting" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Import Settings</h4>
            </div>
            <div class="modal-body">
              
              <div class="form-group">
                <label class="control-label col-md-12" for="import-code">Put your backup code bellow:</label>
                <div class="col-md-12">
                  <textarea rows="10" class="form-control" required="" id="import-code"></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                 Import             </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div id="importDefault" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Restore Default Settings</h4>
            </div>
            <div class="modal-body">
              
              <div class="form-group">
                <div class="col-md-12">
                  <h4>Are you sure?</h4>
                  <p>All of your settings will replaced with the default.</p>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                 Restore              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    
  </div>
</div>