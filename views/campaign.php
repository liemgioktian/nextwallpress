<div class="wrap">
  <h1>Campaigns</h1><div class="notice notice-info"><p>Make Google love your images with Image Sitemap Pro <a href="https://udinra.com/downloads/udinra-image-sitemap-pro">Know More</a> | <a href="?udinra_image_admin_ignore=0">Hide Notice</a></p></div>

  <div class="bootstrap-wrapper" id="mwp-campaign-list" style="margin-top: 15px;">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-info">
          <div class="panel-body">
            <div id="button-action" align="right" style="margin-bottom: 15px;">
              <a class="btn btn-primary" data-toggle="modal" data-target="#newCampaign">Add New</a>
              <a class="btn btn-success" data-toggle="modal" data-target="#importCampaign"><i class="fa fa-arrow-circle-o-left"></i> Import</a>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="box-content">
                  <table class="table table-striped table-bordered bootstrap-datatable datatable" id="mwpCampaign">
                    <thead>
                      <tr>
                        <th>Campaign</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="center">Sofa Bed</td>
                        <td class="text-center">
                          <span class="label label-success">Active</span>
                          
                        </td>
                        <td class="text-center">
                          <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#editCampaign" title="Edit This Campaign">
                            <i class="fa fa-edit"></i>
                          </button>
                          <a class="btn btn-xs btn-info" title="Manage This Campaign" href="?page=magic-wallpress-campaign&amp;manage_campaign=1">
                            <i class="fa fa-sliders"></i>
                          </a>
                          <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#exportCampaign" title="Export This Campaign">
                            <i class="fa fa-sign-out"></i>
                          </button>
                          <button type="button" class="btn btn-xs btn-danger" title="Delete this Campaign" data-toggle="modal" data-target="#deleteCampaign">
                            <i class="fa fa-trash-o"></i>
                          </button>
                        </td>
                      </tr><tr>
                        <td class="center">Sleeper Sofa</td>
                        <td class="text-center">
                          <span class="label label-warning">Not active</span>
                          
                        </td>
                        <td class="text-center">
                          <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#editCampaign" title="Edit This Campaign">
                            <i class="fa fa-edit"></i>
                          </button>
                          <a class="btn btn-xs btn-info" title="Manage This Campaign" href="?page=magic-wallpress-campaign&amp;manage_campaign=2">
                            <i class="fa fa-sliders"></i>
                          </a>
                          <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#exportCampaign" title="Export This Campaign">
                            <i class="fa fa-sign-out"></i>
                          </button>
                          <button type="button" class="btn btn-xs btn-danger" title="Delete this Campaign" data-toggle="modal" data-target="#deleteCampaign">
                            <i class="fa fa-trash-o"></i>
                          </button>
                        </td>
                      </tr><tr>
                        <td class="center">Sectional Sofa</td>
                        <td class="text-center">
                          <span class="label label-warning">Not active</span>
                          
                        </td>
                        <td class="text-center">
                          <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#editCampaign" title="Edit This Campaign">
                            <i class="fa fa-edit"></i>
                          </button>
                          <a class="btn btn-xs btn-info" title="Manage This Campaign" href="?page=magic-wallpress-campaign&amp;manage_campaign=3">
                            <i class="fa fa-sliders"></i>
                          </a>
                          <button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#exportCampaign" title="Export This Campaign">
                            <i class="fa fa-sign-out"></i>
                          </button>
                          <button type="button" class="btn btn-xs btn-danger" title="Delete this Campaign" data-toggle="modal" data-target="#deleteCampaign">
                            <i class="fa fa-trash-o"></i>
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="newCampaign" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">New Campaign</h4>
            </div>

            <div class="modal-body">
              
              <div>
              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-name">Name</label>
                <div class="col-md-9">
                  <input type="text" id="campaign-name" class="form-control" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-active">Status</label>
                <div class="col-md-9">
                  <select id="campaign-active" class="form-control">
                    <option value="1">Active</option>
                    <option value="0">Not Active</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-post-title">Post Title Format</label>
                <div class="col-md-9">
                  <input type="text" id="campaign-post-title" class="form-control" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-image-title">Attachment Title Format</label>
                <div class="col-md-9">
                  <input type="text" id="campaign-image-title" class="form-control" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-image-alt">Image Alt Format</label>
                <div class="col-md-9">
                  <input type="text" id="campaign-image-alt" class="form-control" required="">
                  <span class="help-block"><strong>Use attachment shortcode</strong></span>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-image-size">Image Size</label>
                <div class="col-md-9">
                  <select id="campaign-image-size" class="form-control">
                    <option value="any">Any</option>
                    <option value="small">Small</option>
                    <option value="medium">Medium</option>
                    <option value="large">Large</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-post-template">Post Template</label>
                <div class="col-md-9">
                  <select class="form-control" id="campaign-post-template" multiple="multiple">
                    <option value="3">Atigan Post 20</option><option value="4">Atigan Post 30</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-attachment-template">Attachment Template</label>
                <div class="col-md-9">
                  <select class="form-control" id="campaign-attachment-template" multiple="multiple">
                    <option value="1">Atigan Attachment 3</option><option value="5">Atigan Attachment 4</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-image_caption-template">Image Caption Template</label>
                <div class="col-md-9">
                  <select class="form-control" id="campaign-image_caption-template" multiple="multiple">
                    <option value="2">Atigan Caption 3</option><option value="6">Atigan Caption 4</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-categories">Post Categories</label>
                <div class="col-md-9">
                  <select class="form-control" id="campaign-categories" multiple="multiple">
                    <option value="3">Living Room</option><option value="11">Sectional Sofa</option><option value="1">Sleeper Sofa</option><option value="2">Sofa Bed</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="campaign-authors">Authors</label>
                <div class="col-md-9">
                  <select class="form-control" id="campaign-authors" multiple="multiple">
                    <option value="1">el</option>
                  </select>
                </div>
              </div>
            </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Create</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="editCampaign" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Campaign ""</h4>
            </div>

            <div class="modal-body">
              
              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-name">Name</label>
                <div class="col-md-9">
                  <input type="text" id="edit-campaign-name" class="form-control" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-active">Status</label>
                <div class="col-md-9">
                  <select id="edit-campaign-active" class="form-control">
                    <option value="1">Active</option>
                    <option value="0">Not Active</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-post-title">Post Title Format</label>
                <div class="col-md-9">
                  <input type="text" id="edit-campaign-post-title" class="form-control" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-image-title">Attachment Title Format</label>
                <div class="col-md-9">
                  <input type="text" id="edit-campaign-image-title" class="form-control" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-image-alt">Image Alt Format</label>
                <div class="col-md-9">
                  <input type="text" id="edit-campaign-image-alt" class="form-control" required="">
                  <span class="help-block"><strong>Use attachment shortcode</strong></span>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-image-size">Image Size</label>
                <div class="col-md-9">
                  <select id="edit-campaign-image-size" class="form-control">
                    <option value="any">Any</option>
                    <option value="small">Small</option>
                    <option value="medium">Medium</option>
                    <option value="large">Large</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-post-template">Post Template</label>
                <div class="col-md-9">
                  <select class="form-control" id="edit-campaign-post-template" multiple="multiple">
                    <option value="3">Atigan Post 20</option><option value="4">Atigan Post 30</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-attachment-template">Attachment Template</label>
                <div class="col-md-9">
                  <select class="form-control" id="edit-campaign-attachment-template" multiple="multiple">
                    <option value="1">Atigan Attachment 3</option><option value="5">Atigan Attachment 4</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-image_caption-template">Image Caption Template</label>
                <div class="col-md-9">
                  <select class="form-control" id="edit-campaign-image_caption-template" multiple="multiple">
                    <option value="2">Atigan Caption 3</option><option value="6">Atigan Caption 4</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-categories">Post Categories</label>
                <div class="col-md-9">
                  <select class="form-control" id="edit-campaign-categories" multiple="multiple">
                    <option value="3">Living Room</option><option value="11">Sectional Sofa</option><option value="1">Sleeper Sofa</option><option value="2">Sofa Bed</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="edit-campaign-authors">Authors</label>
                <div class="col-md-9">
                  <select class="form-control" id="edit-campaign-authors" multiple="multiple">
                    <option value="1">el</option>
                  </select>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Update</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="deleteCampaign" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Delete this campaign ""</h4>
            </div>

            <div class="modal-body">
              
              <div class="form-group">
                <label class="control-label col-md-3" for="delete-post">Also Delete Post</label>
                <div class="col-md-9">
                  <select id="delete-post" class="form-control">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                </div>
              </div>

              

            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-danger">
                <span>Delete</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="exportCampaign" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title" id="delete-campaign-title">Export campaign ""</h4>
            </div>

            <div class="modal-body">
              <div class="form-group">
                <label class="control-label col-md-3" for="export-type">Exported Data</label>
                <div class="col-md-9">
                  <select id="export-type" class="form-control">
                    <option value="keywords_list">Keywords List</option>
                    <option value="backup_code">Backup Code</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="export-include_keywords">Include Keywords</label>
                <div class="col-md-9">
                  <select id="export-include_keywords" class="form-control">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                </div>
              </div>
              
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Export</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="importCampaign" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Import Campaign</h4>
            </div>
            <div class="modal-body">
              
              <div class="form-group">
                <label class="control-label col-md-12" for="import-code">Put your backup code bellow:</label>
                <div class="col-md-12">
                  <textarea rows="10" class="form-control" required="" id="import-code"></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                <span>Import</span>
                
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>

</div>