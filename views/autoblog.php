<div class="wrap">
  <h1>AutoBlog Settings</h1><div class="notice notice-info"><p>Make Google love your images with Image Sitemap Pro <a href="https://udinra.com/downloads/udinra-image-sitemap-pro">Know More</a> | <a href="?udinra_image_admin_ignore=0">Hide Notice</a></p></div>
  <div class="bootstrap-wrapper" id="mwp-autoblog" style="margin-top: 15px;">
    <form action="" method="post" class="form-horizontal form-bordered">
      <div class="row">
        <div class="col-md-4">
          <div class="panel panel-info">
            <div class="panel-heading">
              <h2 class="panel-title">Cron URL</h2>
            </div>
            <div class="panel-body">
              <div class="form-group">
                <div class="col-md-12">
                  <input id="attachment-every" type="text" class="form-control">
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-info">
            <div class="panel-heading">
              <h2 class="panel-title">Documentation</h2>
            </div>
            <div class="panel-body">
              <p>Read the documentation for learn more about Autoblog and how to setting up cron job.</p>
              <a class="btn btn-info btn-block" href="http://insfires.com/docs/magic-wallpress#autoblog" target="_blank">Open Documentation</a>
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="panel panel-info">
            <div class="panel-body">
              <button type="submit" class="btn btn-primary pull-right">
                 Update             </button>
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#auto-post">Auto Post</a></li>
                <li><a data-toggle="tab" href="#auto-attachment">Auto Attachment</a></li>
              </ul>
              <div class="tab-content" style="margin-top: 20px">
                

                <div id="auto-post" class="tab-pane fade in active">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="post-every">Create Post Every</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="post-every" type="number" class="form-control">
                        <span class="input-group-addon">Minutes</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="post-random_min_minute">Random Additional Minutes</label>
                    <div class="col-md-9">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="post-random_min_minute" type="number" class="form-control">
                            <span class="input-group-addon">Minutes (Min)</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="post-random_max_minute" type="number" class="form-control">
                            <span class="input-group-addon">Minutes (Max)</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="post-random_min_second">Random Additional Seconds</label>
                    <div class="col-md-9">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="post-random_min_second" type="number" class="form-control">
                            <span class="input-group-addon">Seconds (Min)</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="post-random_max_second" type="number" class="form-control">
                            <span class="input-group-addon">Seconds (Max)</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div id="auto-attachment" class="tab-pane fade">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="attachment-every">Insert Attachment Every</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="attachment-every" type="number" class="form-control">
                        <span class="input-group-addon">Minutes</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="attachment-max_attachment">Max Attachments</label>
                    <div class="col-md-9">
                      <div class="input-group">
                        <input id="attachment-max_attachment" type="number" class="form-control">
                        <span class="input-group-addon">Image per Post</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="attachment-random_min_minute">Random Additional Minutes</label>
                    <div class="col-md-9">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="attachment-random_min_minute" type="number" class="form-control">
                            <span class="input-group-addon">Minutes (Min)</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="attachment-random_max_minute" type="number" class="form-control">
                            <span class="input-group-addon">Minutes (Max)</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="attachment-random_min_second">Random Additional Seconds</label>
                    <div class="col-md-9">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="attachment-random_min_second" type="number" class="form-control">
                            <span class="input-group-addon">Seconds (Min)</span>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="input-group">
                            <input id="attachment-random_max_second" type="number" class="form-control">
                            <span class="input-group-addon">Seconds (Max)</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>