<div class="wrap">
  <h2>Templates</h2><div class="notice notice-info"><p>Make Google love your images with Image Sitemap Pro <a href="https://udinra.com/downloads/udinra-image-sitemap-pro">Know More</a> | <a href="?udinra_image_admin_ignore=0">Hide Notice</a></p></div>
  <div class="bootstrap-wrapper" id="mwp-template-list" style="margin-top: 15px;">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-info">
          <div class="panel-body">
            <a class="btn btn-primary pull-right" data-toggle="modal" data-target="#newTemplate">Add New</a>
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#post">Post</a></li>
              <li><a data-toggle="tab" href="#attachment">Attachment</a></li>
              <li><a data-toggle="tab" href="#image_caption">Image Caption</a></li>
              <li><a data-toggle="tab" href="#importExport">Import &amp; Export</a></li>
            </ul>

            <div class="tab-content" style="margin-top: 20px">
              
              <div id="post" class="tab-pane fade in active">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Note</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="center">Atigan Post 20</td>
                        <td class="center">
                          Atigan Post 3
                        </td>
                        <td class="text-center">
                          <a href="#" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateTemplate"><i class="fa fa-sliders" title="Quick edit this template"></i></a>
                          <a class="btn btn-xs btn-success" title="Edit this template" href="?page=magic-wallpress-template&amp;template_id=3"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-xs btn-danger" title="Delete this template" data-toggle="modal" data-target="#deleteTemplate"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr><tr>
                        <td class="center">Atigan Post 30</td>
                        <td class="center">
                          Atigan Post 4
                        </td>
                        <td class="text-center">
                          <a href="#" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateTemplate"><i class="fa fa-sliders" title="Quick edit this template"></i></a>
                          <a class="btn btn-xs btn-success" title="Edit this template" href="?page=magic-wallpress-template&amp;template_id=4"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-xs btn-danger" title="Delete this template" data-toggle="modal" data-target="#deleteTemplate"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
              </div>

              <div id="attachment" class="tab-pane fade">
                  <table class="table table-striped table-bordered bootstrap-datatable datatable" id="mwpCampaign">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Note</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="center">Atigan Attachment 3</td>
                        <td class="center">
                          Atigan Attachment 3
                        </td>
                        <td class="text-center">
                          <a href="#" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateTemplate"><i class="fa fa-sliders" title="Quick edit this template"></i></a>
                          <a class="btn btn-xs btn-success" title="Edit this template" href="?page=magic-wallpress-template&amp;template_id=1"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-xs btn-danger" title="Delete this template" data-toggle="modal" data-target="#deleteTemplate"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr><tr>
                        <td class="center">Atigan Attachment 4</td>
                        <td class="center">
                          Atigan Attachment 4
                        </td>
                        <td class="text-center">
                          <a href="#" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateTemplate"><i class="fa fa-sliders" title="Quick edit this template"></i></a>
                          <a class="btn btn-xs btn-success" title="Edit this template" href="?page=magic-wallpress-template&amp;template_id=5"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-xs btn-danger" title="Delete this template" data-toggle="modal" data-target="#deleteTemplate"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
              </div>

              <div id="image_caption" class="tab-pane fade">
                  <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th>Note</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="center">Atigan Caption 3</td>
                        <td class="center">
                          Atigan Caption 3
                        </td>
                        <td class="text-center">
                          <a href="#" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateTemplate"><i class="fa fa-sliders" title="Quick edit this template"></i></a>
                          <a class="btn btn-xs btn-success" title="Edit this template" href="?page=magic-wallpress-template&amp;template_id=2"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-xs btn-danger" title="Delete this template" data-toggle="modal" data-target="#deleteTemplate"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr><tr>
                        <td class="center">Atigan Caption 4</td>
                        <td class="center">
                          Atigan Caption 4
                        </td>
                        <td class="text-center">
                          <a href="#" class="btn btn-xs btn-info" data-toggle="modal" data-target="#updateTemplate"><i class="fa fa-sliders" title="Quick edit this template"></i></a>
                          <a class="btn btn-xs btn-success" title="Edit this template" href="?page=magic-wallpress-template&amp;template_id=6"><i class="fa fa-edit"></i></a>
                          <a href="#" class="btn btn-xs btn-danger" title="Delete this template" data-toggle="modal" data-target="#deleteTemplate"><i class="fa fa-trash-o"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
              </div>
              
              <div id="importExport" class="tab-pane fade">
                <form class="form-horizontal form-bordered">
                  <div class="form-group">
                    <label class="control-label col-md-3" for="importText">Import Templates</label>
                    <div class="col-md-9">
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importCode"><i class="fa fa-arrow-circle-o-left"></i> Import from code</button>
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#importDefault"><i class="fa fa-undo"></i> Import default</button>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3" for="exportText">Backup Templates</label>
                    <div class="col-md-9">
                      <span class="help-block"><strong>Text bellow is backup code for saved templates</strong></span>
                      <textarea rows="8" class="form-control" id="exportText">YTo2OntpOjA7YTo0OntzOjQ6Im5hbWUiO3M6MTk6IkF0aWdhbiBBdHRhY2htZW50IDMiO3M6NDoidHlwZSI7czoxMDoiYXR0YWNobWVudCI7czo0OiJub3RlIjtzOjE5OiJBdGlnYW4gQXR0YWNobWVudCAzIjtzOjc6ImNvbnRlbnQiO3M6Mjk0MzoiWyVGcm9tIHRoZXxUaHJvdWdoIHRoZSVdIFsldGhvdXNhbmR8dGhvdXNhbmRzIG9mJV0gWyVpbWFnZXN8cGljdHVyZXN8cGhvdG9zfHBob3RvZ3JhcGhzJV0gWyVvbiB0aGUgaW50ZXJuZXR8b24gdGhlIHdlYnxvbiB0aGUgbmV0fG9ubGluZXxvbi1saW5lJV0gWyVhYm91dHxyZWdhcmRpbmd8Y29uY2VybmluZ3x3aXRoIHJlZ2FyZHMgdG98aW4gcmVsYXRpb24gdG8lXSA8c3Ryb25nPnt7a2V5d29yZH19PC9zdHJvbmc+LCBbJXdlfHdlIGFsbHwlXSBbJXBpY2tzfGNob2ljZXN8c2VsZWN0cyVdIFsldGhlIGJlc3R8dGhlIHZlcnkgYmVzdHx0aGUgdG9wJV0gWyVjb2xsZWN0aW9uc3xzZWxlY3Rpb25zfGNob2ljZXN8c2VyaWVzfGxpYnJhcmllcyVdIFsld2l0aHxhbG9uZyB3aXRofHRvZ2V0aGVyIHdpdGh8dXNpbmd8aGF2aW5nJV0gWyViZXN0fGdyZWF0ZXN0fGlkZWFsJV0gWyVyZXNvbHV0aW9ufHF1YWxpdHl8aW1hZ2UgcmVzb2x1dGlvbiVdIFslanVzdCBmb3J8c2ltcGx5IGZvcnxvbmx5IGZvcnxleGNsdXNpdmVseSBmb3IlXSBbJXlvdXx5b3UgYWxsJV0sIFslYW5kIHRoaXN8YW5kIG5vdyB0aGlzJV0gWyVpbWFnZXN8cGljdHVyZXN8cGhvdG9zfHBob3RvZ3JhcGhzJV0gWyVpc3xpcyBhY3R1YWxseXxpcyB1c3VhbGx5JV0gWyVvbmUgb2Z8YW1vbmd8Y29uc2lkZXJlZCBvbmUgb2Z8b25lIGFtb25nJV0gWyVpbWFnZXN8cGljdHVyZXN8cGhvdG9zfHBob3RvZ3JhcGhzfGdyYXBoaWNzJV0gWyVjb2xsZWN0aW9uc3xzZWxlY3Rpb25zfGNob2ljZXN8c2VyaWVzfGxpYnJhcmllcyVdIFslaW4gb3VyfHdpdGhpbiBvdXJ8aW5zaWRlIG91cnxpbiB5b3VyfGluIHRoaXMlXSBbJWJlc3R8Z3JlYXRlc3R8ZmluZXN0fGlkZWFsfHZlcnkgYmVzdCVdIFslaW1hZ2VzfHBpY3R1cmVzfHBob3Rvc3xwaG90b2dyYXBoc3xncmFwaGljcyVdIGdhbGxlcnkgWyVhYm91dHxyZWdhcmRpbmd8Y29uY2VybmluZ3x3aXRoIHJlZ2FyZHMgdG98aW4gcmVsYXRpb24gdG8lXSA8ZW0+e3twb3N0LnBvc3RfdGl0bGV9fTwvZW0+LiBbJUkgaG9wZXxJIHJlYWxseSBob3BlfEkgYW0gaG9waW5nfEknbSBob3Bpbmd8TGV0cyBob3BlJV0gWyV5b3Ugd2lsbHx5b3UnbGx8eW91IG1heXx5b3UgY2FufHlvdSBtaWdodCVdIFslbGlrZSBpdHxlbmpveSBpdHx3YW50IGl0fGFzIGl0fHRoaW5rIGl0J3MgZ3JlYXQlXS4NCg0KWyVUaGlzfFRoaXMgcGFydGljdWxhcnxUaGlzIGtpbmQgb2Z8VGhpcyBzcGVjaWZpY3xUaGF0JV0gWyVpbWFnZXxwaWN0dXJlfGdyYXBoaWN8aW1wcmVzc2lvbnxwaG90b2dyYXBoJV0gKDxlbT57e2F0dGFjaG1lbnQucG9zdF90aXRsZX19PC9lbT4pIFslYWJvdmV8b3ZlcnxwcmV2aW91c2x5IG1lbnRpb25lZHxlYXJsaWVyIG1lbnRpb25lZHxwcmVjZWRpbmclXSBbJWlzfGlzIGFjdHVhbGx5fHdpbGwgYmV8Y2FuIGJlfGlzIHVzdWFsbHklXSBbJWxhYmVsbGVkfGJyYW5kZWR8Y2xhc3NlZCVdIFsld2l0aHxhbG9uZyB3aXRofHRvZ2V0aGVyIHdpdGh8dXNpbmd8aGF2aW5nJV06IHslIHNldCB0YWdzID0gZ2V0X3RhZ3MoMSwzLHBvc3QuSUQpICV9eyUgaWYgdGFncyAlfXslIGZvciB0YWcgaW4gdGFncyAlfXt7dGFnLm5hbWV9fSwgeyUgZW5kZm9yICV9LnslIGVuZGlmICV9DQoNClslcG9zdGVkfHN1Ym1pdHRlZHxwdWJsaXNoZWR8cHV0IHVwfHBsYWNlZCVdIFslYnl8dGhyb3VnaHxzaW1wbHkgYnl8Ynkgc2ltcGx5fGJ5IG1lYW5zIG9mJV0ge3twb3N0LmF1dGhvcl9uYW1lfX0gWyVhdHxmcm9tfHdpdGh8aW58b24lXSB7e3Bvc3QuZGF0ZX19LiBbJVRvIHNlZXxUbyBkZXRlcm1pbmV8VG8gZmluZCBvdXR8VG8gdmlld3xUbyBkaXNjb3ZlciVdIFslYWxsfGp1c3QgYWJvdXQgYWxsfGFsbW9zdCBhbGx8bW9zdHxtYW55JV0gWyVpbWFnZXN8cGljdHVyZXN8cGhvdG9zfHBob3RvZ3JhcGhzfGdyYXBoaWNzJV0gWyVpbnx3aXRoaW58aW5zaWRlfHRocm91Z2hvdXR8d2l0aCVdIHt7cG9zdC5wb3N0X3RpdGxlfX0gWyVpbWFnZXN8cGljdHVyZXN8cGhvdG9zfHBob3RvZ3JhcGhzfGdyYXBoaWNzJV0gZ2FsbGVyeSBbJXBsZWFzZXxtYWtlIHN1cmUgeW91fHlvdSBzaG91bGR8cmVtZW1iZXIgdG98eW91IG5lZWQgdG8lXSBbJWZvbGxvd3xhZGhlcmUgdG98c3RpY2sgdG98Y29tcGx5IHdpdGh8YWJpZGUgYnklXSA8YSBocmVmPSJ7e3Bvc3QudXJsfX0iIHRpdGxlPSJ7e3Bvc3QucG9zdF90aXRsZX19Ij5bJXRoaXN8dGhpcyBwYXJ0aWN1bGFyfHRoaXMga2luZCBvZnx0aGlzIHNwZWNpZmljfHRoYXQlXSBbJWxpbmt8aHlwZXJsaW5rfHdlYnNpdGUgbGlua3x1cmx8d2ViIHBhZ2UgbGluayVdPC9hPi4NCg0KWyVbJVRoZSBNb3N0IHxUaGUgfCAlXVslRWxlZ2FudHxTdHlsaXNofEF3ZXNvbWV8QW1hemluZ3xCcmlsbGlhbnR8SW5jcmVkaWJsZSVdIFslWyVhbmR8YXMgd2VsbCBhc3xhbmQgYWxzb3xhbG9uZyB3aXRofGluIGFkZGl0aW9uIHRvJV0gWyVBdHRyYWN0aXZlfEJlYXV0aWZ1bHxTdHVubmluZ3xHb3JnZW91c3xMb3ZlbHl8SW50ZXJlc3RpbmclXSB8ICVdfCAlXXt7a2V5d29yZHx0aXRsZX19IFslZm9yfHdpdGggcmVnYXJkIHRvfHJlZ2FyZGluZ3xwZXJ0YWluaW5nIHRvfGludGVuZGVkIGZvciVdIFslWyVJbnNwaXJlfEVuY291cmFnZXxNb3RpdmF0ZXxJbnZpZ29yYXRlfFJlYWxseSBlbmNvdXJhZ2UlXXxbJVlvdXIgaG9tZXxZb3VyIGhvdXNlfFlvdXIgcHJvcGVydHl8WW91ciBvd24gaG9tZXxUaGUgaG91c2UlXXxbJVslUHJlc2VudHxFeGlzdGluZ3xDdXJyZW50fEZvdW5kfFByb3ZpZGUlXXwlXSBbJUhvbWV8SG91c2V8UmVzaWRlbmNlfEhvdXNlaG9sZHxQcm9wZXJ0eSVdfFslWyVDb3p5fENvbWZvcnRhYmxlfENvbWZ5fFdhcm18SW52aXRpbmclXXxbJURyZWFtfERlc2lyZXxGYW50YXN5fEFzcGlyYXRpb258V2lzaCVdJV18WyVIb21lfEhvdXNlfFJlc2lkZW5jZXxIb3VzZWhvbGR8UHJvcGVydHklXSVdIjt9aToxO2E6NDp7czo0OiJuYW1lIjtzOjE2OiJBdGlnYW4gQ2FwdGlvbiAzIjtzOjQ6InR5cGUiO3M6MTM6ImltYWdlX2NhcHRpb24iO3M6NDoibm90ZSI7czoxNjoiQXRpZ2FuIENhcHRpb24gMyI7czo3OiJjb250ZW50IjtzOjU1OiJ7e2F0dGFjaG1lbnQucG9zdF90aXRsZX19wqB7e2F0dGFjaG1lbnQuc291cmNlX2RvbWFpbn19Ijt9aToyO2E6NDp7czo0OiJuYW1lIjtzOjE0OiJBdGlnYW4gUG9zdCAyMCI7czo0OiJ0eXBlIjtzOjQ6InBvc3QiO3M6NDoibm90ZSI7czoxMzoiQXRpZ2FuIFBvc3QgMyI7czo3OiJjb250ZW50IjtzOjU5NjI6IjxzdHJvbmc+e3twb3N0LnBvc3RfdGl0bGV9fTwvc3Ryb25nPiB8IFslV2VsY29tZXxFbmNvdXJhZ2VkfFBsZWFzYW50fERlbGlnaHRmdWx8QWxsb3dlZCVdIFsldG98aW4gb3JkZXIgdG98dG8gYmUgYWJsZSB0b3xmb3IgeW91IHRvfHRvIGhlbHAlXSBbJW15fG15IHBlcnNvbmFsfG15IG93bnxvdXJ8dGhlJV0gWyVibG9nfHdlYmxvZ3x3ZWJzaXRlfHdlYnNpdGV8YmxvZyBzaXRlfGJsb2clXSwgWyVpbiB0aGlzfHdpdGggdGhpc3xvbiB0aGlzfHdpdGhpbiB0aGlzfGluIHRoaXMgcGFydGljdWxhciVdIFsldGltZXxwZXJpb2R8bW9tZW50fG9jY2FzaW9ufHRpbWUgcGVyaW9kJV0gWyVJXCdsbHxJIHdpbGx8SSBhbSBnb2luZyB0b3xJXCdtIGdvaW5nIHRvfFdlXCdsbCVdIFslc2hvdyB5b3V8ZGVtb25zdHJhdGV8ZXhwbGFpbiB0byB5b3V8dGVhY2ggeW91fHByb3ZpZGUgeW91IHdpdGglXSBbJWFib3V0fHJlZ2FyZGluZ3xjb25jZXJuaW5nfHdpdGggcmVnYXJkcyB0b3xpbiByZWxhdGlvbiB0byVdIHt7a2V5d29yZH19LiBbJUFuZCBub3d8QW5kIHRvZGF5fE5vd3xBbmQgYWZ0ZXIgdGhpc3xBbmQgZnJvbSBub3cgb24lXSwgWyV0aGlzIGlzIHRoZXx0aGlzIGlzIGFjdHVhbGx5IHRoZXxoZXJlIGlzIHRoZXx0aGlzIGNhbiBiZSBhfHRoaXMgY2FuIGJlIHRoZSVdIFslZmlyc3R8dmVyeSBmaXJzdHxpbml0aWFsfDFzdHxwcmltYXJ5JV0gWyVpbWFnZXxwaWN0dXJlfGdyYXBoaWN8aW1wcmVzc2lvbnxwaG90b2dyYXBoJV06DQoNCjxzcGFuPnslIHNldCBhdHRhY2htZW50cyA9IGdldF9hdHRhY2htZW50cygxLDIscG9zdC5JRCkgJX08L3NwYW4+DQo8c3Bhbj57JSBpZiBhdHRhY2htZW50cyAlfTwvc3Bhbj4NCjxzcGFuPnslIGZvciBhdHRhY2htZW50IGluIGF0dGFjaG1lbnRzICV9PC9zcGFuPg0KPHNwYW4+W213cF9odG1sIHRhZz1cImltZ1wiIHNyYz1cInt7YXR0YWNobWVudC5pbWFnZV9zcmNfZnVsbH19XCIgYWx0PVwie3thdHRhY2htZW50LmltYWdlX2FsdH19XCJdDQp7e2F0dGFjaG1lbnQucG9zdF9leGNlcnB0fX08L3NwYW4+DQo8c3Bhbj57JSBlbmRmb3IgJX08L3NwYW4+DQo8c3Bhbj57JSBlbmRpZiAlfTwvc3Bhbj4NCg0KWyVIb3cgYWJvdXR8V2hhdCBhYm91dHxUaGluayBhYm91dHxXaHkgZG9uXCd0IHlvdSBjb25zaWRlcnxXaHkgbm90IGNvbnNpZGVyJV0gWyVpbWFnZXxwaWN0dXJlfGdyYXBoaWN8aW1wcmVzc2lvbnxwaG90b2dyYXBoJV0gWyVhYm92ZXxvdmVyfHByZXZpb3VzbHkgbWVudGlvbmVkfGVhcmxpZXIgbWVudGlvbmVkfHByZWNlZGluZyVdPyBbJWlzfGlzIGFjdHVhbGx5fHdpbGwgYmV8Y2FuIGJlfGlzIHVzdWFsbHklXSBbJXRoYXR8d2hpY2h8aW4gd2hpY2h8dGhhdCB3aWxsfG9mIHdoaWNoJV0gWyVhbWF6aW5nfGluY3JlZGlibGV8YXdlc29tZXxyZW1hcmthYmxlfHdvbmRlcmZ1bCVdPz8/LiBbJWlmIHlvdSB0aGlua3xpZiB5b3UgZmVlbHxpZiB5b3UgYmVsaWV2ZXxpZiB5b3UgdGhpbmsgbWF5YmV8aWYgeW91XCdyZSBtb3JlIGRlZGljYXRlZCVdIFslc298dGhlcmVmb3JlfHRodXN8Y29uc2VxdWVudGx5fGFuZCBzbyVdLCBJXCdsIFslbHxtfGR8dCVdIFslc2hvdyB5b3V8ZGVtb25zdHJhdGV8ZXhwbGFpbiB0byB5b3V8dGVhY2ggeW91fHByb3ZpZGUgeW91IHdpdGglXSBbJXNvbWV8YSBmZXd8c2V2ZXJhbHxhIG51bWJlciBvZnxtYW55JV0gWyVpbWFnZXxwaWN0dXJlfGdyYXBoaWN8aW1wcmVzc2lvbnxwaG90b2dyYXBoJV0gWyVhZ2FpbnxvbmNlIGFnYWlufG9uY2UgbW9yZXx5ZXQgYWdhaW58YWxsIG92ZXIgYWdhaW4lXSBbJWJlbG93fGJlbmVhdGh8dW5kZXJ8ZG93biBiZWxvd3x1bmRlcm5lYXRoJV06DQo8cCBzdHlsZT1cInRleHQtYWxpZ246IGxlZnQ7XCI+PHNwYW4+eyUgc2V0IGF0dGFjaG1lbnRzID0gZ2V0X2F0dGFjaG1lbnRzKDMsMjAscG9zdC5JRCkgJX08L3NwYW4+DQo8c3Bhbj57JSBpZiBhdHRhY2htZW50cyAlfTwvc3Bhbj4NCjxzcGFuPnslIGZvciBhdHRhY2htZW50IGluIGF0dGFjaG1lbnRzICV9PC9zcGFuPg0KPHNwYW4+W213cF9odG1sIHRhZz1cImltZ1wiIHNyYz1cInt7YXR0YWNobWVudC5pbWFnZV9zcmNfZnVsbH19XCIgYWx0PVwie3thdHRhY2htZW50LmltYWdlX2FsdH19XCJdDQp7e2F0dGFjaG1lbnQucG9zdF9leGNlcnB0fX08L3NwYW4+DQo8c3Bhbj57JSBlbmRmb3IgJX08L3NwYW4+DQo8c3Bhbj57JSBlbmRpZiAlfTwvc3Bhbj48L3A+DQpTbywgWyVpZiB5b3Ugd2FudCB0b3xpZiB5b3Ugd2lzaCB0b3xpZiB5b3Ugd291bGQgbGlrZXxpZiB5b3VcJ2QgbGlrZSB0b3xpZiB5b3UgZGVzaXJlIHRvfGlmIHlvdSBsaWtlIHRvJV0gWyVnZXR8cmVjZWl2ZXxoYXZlfG9idGFpbnxhY3F1aXJlfHNlY3VyZSVdIFsldGhlc2V8YWxsIHRoZXNlfGFsbCBvZiB0aGVzZXx0aGUlXSBbJWF3ZXNvbWV8YW1hemluZ3xncmVhdHx3b25kZXJmdWx8bWFnbmlmaWNlbnR8ZmFudGFzdGljfGluY3JlZGlibGV8b3V0c3RhbmRpbmclXSBbJWltYWdlc3xwaG90b3N8cGljc3xwaWN0dXJlc3xncmFwaGljc3xzaG90cyVdIFslYWJvdXR8cmVnYXJkaW5nfHJlbGF0ZWQgdG8lXSAoe3twb3N0LnBvc3RfdGl0bGV9fSksIFslanVzdCBjbGlja3xzaW1wbHkgY2xpY2t8Y2xpY2sgb258cHJlc3N8Y2xpY2slXSBzYXZlIFslYnV0dG9ufGxpbmt8aWNvbiVdIFsldG8gc2F2ZXx0byBzdG9yZXx0byBkb3dubG9hZCVdIFsldGhlc2V8dGhlJV0gWyVpbWFnZXN8cGhvdG9zfHBpY3N8cGljdHVyZXN8Z3JhcGhpY3N8c2hvdHMlXSBbJXRvIHlvdXJ8Zm9yIHlvdXJ8aW4geW91ciVdIFslY29tcHV0ZXJ8cGN8cGVyc29uYWwgY29tcHV0ZXJ8bGFwdG9wfHBlcnNvbmFsIHBjJV0uIFslVGhleSBhcmV8VGhleVwncmV8VGhlc2UgYXJlfFRoZXJlXCdyZSVdIFslcmVhZHl8cHJlcGFyZWR8YWxsIHNldHxhdmFpbGFibGUlXSBmb3IgWyVkb3dubG9hZHxvYnRhaW58ZG93biBsb2FkfHNhdmV8dHJhbnNmZXIlXSwgWyVpZiB5b3UgbGlrZXxpZiB5b3Ugd2FudHxpZiB5b3VcJ2QgcHJlZmVyfGlmIHlvdSBsb3ZlfGlmIHlvdSBhcHByZWNpYXRlfGlmIHlvdVwnZCByYXRoZXIlXSBbJWFuZCB3YW50IHRvfGFuZCB3aXNoIHRvJV0gWyVnZXQgaXR8aGF2ZSBpdHxvYnRhaW4gaXR8b3duIGl0fGdyYWIgaXR8dGFrZSBpdCVdLCBbJWNsaWNrfHNpbXBseSBjbGlja3xqdXN0IGNsaWNrJV0gc2F2ZSBbJWxvZ298c3ltYm9sfGJhZGdlJV0gWyVvbiB0aGV8aW4gdGhlJV0gWyVwYWdlfHdlYiBwYWdlfGFydGljbGV8cG9zdCVdLCBbJWFuZCBpdCB3aWxsfGFuZCBpdFwnbGwlXSBiZSBbJWRpcmVjdGx5fGltbWVkaWF0ZWx5fGluc3RhbnRseSVdIFslZG93bmxvYWRlZHxkb3duIGxvYWRlZHxzYXZlZCVdIFsldG8geW91cnxpbiB5b3VyJV0gWyVjb21wdXRlcnxsYXB0b3B8cGN8ZGVza3RvcCBjb21wdXRlcnxsYXB0b3AgY29tcHV0ZXJ8aG9tZSBjb21wdXRlcnxub3RlYm9vayBjb21wdXRlciVdLiVdIFslWyVGaW5hbGx5fExhc3RseXxBcyBhIGZpbmFsIHBvaW50fEF0IGxhc3QlXSBbJWlmIHlvdSB3YW50IHRvfGlmIHlvdSBkZXNpcmUgdG98aWYgeW91IGxpa2UgdG98aWYgeW91IHdpc2ggdG98aWYgeW91IHdvdWxkIGxpa2V8aWYgeW91XCdkIGxpa2UgdG98aW4gb3JkZXIgdG98aWYgeW91IG5lZWQgdG8lXSBbJWdldHxyZWNlaXZlfGhhdmV8b2J0YWlufHNlY3VyZXxmaW5kfGdyYWJ8Z2FpbiVdIFslbmV3fHVuaXF1ZSVdIFslYW5kIHRoZXxhbmQlXSBbJWxhdGVzdHxyZWNlbnQlXSBbJWltYWdlfGdyYXBoaWN8cGljdHVyZXxwaG90byVdIFslcmVsYXRlZCB3aXRofHJlbGF0ZWQgdG8lXSAoe3twb3N0LnBvc3RfdGl0bGV9fSksIHBsZWFzZSBmb2xsb3cgdXMgb24gZ29vZ2xlIHBsdXMgb3IgWyVib29rbWFya3xzYXZlfGJvb2sgbWFyayVdIFsldGhpcyBzaXRlfHRoaXMgd2Vic2l0ZXx0aGlzIHBhZ2V8dGhpcyBibG9nfHRoZSBzaXRlJV0sIFsld2UgdHJ5fHdlIGF0dGVtcHQlXSBvdXIgYmVzdCBbJXRvIGdpdmUgeW91fHRvIG9mZmVyIHlvdXx0byBwcm92aWRlfHRvIHByZXNlbnQgeW91JV0gWyVkYWlseXxyZWd1bGFyJV0gWyV1cGRhdGV8dXAtZGF0ZXx1cCBncmFkZSVdIHdpdGggWyVmcmVzaCBhbmQgbmV3fGFsbCBuZXcgYW5kIGZyZXNoJV0gWyVpbWFnZXN8Z3JhcGhpY3N8c2hvdHN8cGhvdG9zfHBpY3N8cGljdHVyZXMlXS4gWyVIb3BlIHlvdXxXZSBkbyBob3BlIHlvdSVdIFslZW5qb3l8bG92ZXxsaWtlJV0gWyVzdGF5aW5nfGtlZXBpbmclXSBbJWhlcmV8cmlnaHQgaGVyZSVdLiBbJUZvciBtb3N0fEZvciBtYW55fEZvciBzb21lJV0gWyV1cGRhdGVzfHVwLWRhdGVzfHVwZ3JhZGVzJV0gYW5kIFslbGF0ZXN0fHJlY2VudCVdIFslbmV3c3xpbmZvcm1hdGlvbiVdIGFib3V0ICh7e3Bvc3QucG9zdF90aXRsZX19KSBbJWltYWdlc3xncmFwaGljc3xzaG90c3xwaG90b3N8cGljc3xwaWN0dXJlcyVdLCBwbGVhc2Uga2luZGx5IGZvbGxvdyB1cyBvbiBbJXR3aXR0ZXJ8dHdlZXRzJV0sIHBhdGgsIEluc3RhZ3JhbSBhbmQgZ29vZ2xlIHBsdXMsIG9yIHlvdSBtYXJrIHRoaXMgcGFnZSBvbiBbJWJvb2ttYXJrfGJvb2sgbWFyayVdIFslc2VjdGlvbnxhcmVhJV0sIFslV2UgdHJ5fFdlIGF0dGVtcHQlXSBbJXRvIGdpdmUgeW91fHRvIG9mZmVyIHlvdXx0byBwcmVzZW50IHlvdXx0byBwcm92aWRlIHlvdSB3aXRoJV0gWyV1cGRhdGV8dXAgZ3JhZGV8dXAtZGF0ZSVdIFslcGVyaW9kaWNhbGx5fHJlZ3VsYXJseSVdIHdpdGggWyVmcmVzaCBhbmQgbmV3fGFsbCBuZXcgYW5kIGZyZXNoJV0gWyVpbWFnZXN8Z3JhcGhpY3N8c2hvdHN8cGhvdG9zfHBpY3N8cGljdHVyZXMlXSwgWyVlbmpveXxsaWtlfGxvdmUlXSB5b3VyIFslYnJvd3Npbmd8c2VhcmNoaW5nfGV4cGxvcmluZ3xzdXJmaW5nJV0sIGFuZCBmaW5kIFsldGhlIGJlc3R8dGhlIGlkZWFsfHRoZSBwZXJmZWN0fHRoZSByaWdodCVdIGZvciB5b3UuJV0NCg0KeyUgc2V0IHlvdXR1YmVzID0gZ2V0X3lvdXR1YmVzKDEsMyxrZXl3b3JkKSAlfQ0KeyUgaWYgeW91dHViZXMgJX0NCnslIGZvciB5b3V0dWJlIGluIHlvdXR1YmVzICV9DQoNCltlbWJlZF1odHRwczovL3d3dy55b3V0dWJlLmNvbS9lbWJlZC97e3lvdXR1YmUuaWR9fVsvZW1iZWRdDQoNCnslIGVuZGZvciAlfQ0KeyUgZW5kaWYgJX0NCg0KPHNwYW4+WyVUaGFua3MgZm9yIHZpc2l0aW5nfEhlcmUgeW91IGFyZSBhdCVdIFslb3VyIHNpdGV8b3VyIHdlYnNpdGUlXSwgWyVhcnRpY2xlfGNvbnRlbnQlXWFib3ZlwqAoPGVtPnt7cG9zdC5wb3N0X3RpdGxlfX08L2VtPikgcHVibGlzaGVkIGJ5IHt7cG9zdC5hdXRob3JfbmFtZX19IGF0IHt7cG9zdC5kYXRlfX0uIFslVG9kYXl8Tm93YWRheXN8QXQgdGhpcyB0aW1lJV0gWyV3ZSBhcmV8d2VcJ3JlJV0gWyVkZWxpZ2h0ZWR8cGxlYXNlZHxleGNpdGVkJV0gdG8gWyVhbm5vdW5jZXxkZWNsYXJlJV0gWyV0aGF0IHdlfHdlJV0gWyVoYXZlIGZvdW5kfGhhdmUgZGlzY292ZXJlZCVdIFslYSB2ZXJ5fGFuIGV4dHJlbWVseXxhbiBpbmNyZWRpYmx5fGFuIGF3ZnVsbHklXWludGVyZXN0aW5nIFsldG9waWN8bmljaGV8Y29udGVudCVddG8gYmUgWyVkaXNjdXNzZWR8cmV2aWV3ZWR8cG9pbnRlZCBvdXQlXSwgWyVuYW1lbHl8dGhhdCBpcyVdICg8ZW0+e3twb3N0LnBvc3RfdGl0bGV9fTwvZW0+KSBbJU1hbnkgcGVvcGxlfExvdHMgb2YgcGVvcGxlfE1hbnkgaW5kaXZpZHVhbHN8U29tZSBwZW9wbGV8TW9zdCBwZW9wbGUlXSBbJWxvb2tpbmcgZm9yfHNlYXJjaGluZyBmb3J8dHJ5aW5nIHRvIGZpbmR8YXR0ZW1wdGluZyB0byBmaW5kJV0gWyVpbmZvcm1hdGlvbiBhYm91dHxkZXRhaWxzIGFib3V0fHNwZWNpZmljcyBvZnxpbmZvIGFib3V0JV0oPGVtPnt7cG9zdC5wb3N0X3RpdGxlfX08L2VtPikgWyVhbmQgZGVmaW5pdGVseXxhbmQgY2VydGFpbmx5fGFuZCBvZiBjb3Vyc2UlXSBbJW9uZSBvZiB0aGVtfG9uZSBvZiB0aGVzZSVdIGlzIHlvdSwgaXMgbm90IGl0Pzwvc3Bhbj4iO31pOjM7YTo0OntzOjQ6Im5hbWUiO3M6MTQ6IkF0aWdhbiBQb3N0IDMwIjtzOjQ6InR5cGUiO3M6NDoicG9zdCI7czo0OiJub3RlIjtzOjEzOiJBdGlnYW4gUG9zdCA0IjtzOjc6ImNvbnRlbnQiO3M6NTc3MDoiPHN0cm9uZz57e3Bvc3QucG9zdF90aXRsZX19PC9zdHJvbmc+IHwgWyVXZWxjb21lfEVuY291cmFnZWR8UGxlYXNhbnR8RGVsaWdodGZ1bHxBbGxvd2VkJV0gWyV0b3xpbiBvcmRlciB0b3x0byBiZSBhYmxlIHRvfGZvciB5b3UgdG98dG8gaGVscCVdIFslbXl8bXkgcGVyc29uYWx8bXkgb3dufG91cnx0aGUlXSBbJWJsb2d8d2VibG9nfHdlYnNpdGV8d2Vic2l0ZXxibG9nIHNpdGV8YmxvZyVdLCBbJWluIHRoaXN8d2l0aCB0aGlzfG9uIHRoaXN8d2l0aGluIHRoaXN8aW4gdGhpcyBwYXJ0aWN1bGFyJV0gWyV0aW1lfHBlcmlvZHxtb21lbnR8b2NjYXNpb258dGltZSBwZXJpb2QlXSBbJUlcJ2xsfEkgd2lsbHxJIGFtIGdvaW5nIHRvfElcJ20gZ29pbmcgdG98V2VcJ2xsJV0gWyVzaG93IHlvdXxkZW1vbnN0cmF0ZXxleHBsYWluIHRvIHlvdXx0ZWFjaCB5b3V8cHJvdmlkZSB5b3Ugd2l0aCVdIFslYWJvdXR8cmVnYXJkaW5nfGNvbmNlcm5pbmd8d2l0aCByZWdhcmRzIHRvfGluIHJlbGF0aW9uIHRvJV0ge3trZXl3b3JkfX0uIFslQW5kIG5vd3xBbmQgdG9kYXl8Tm93fEFuZCBhZnRlciB0aGlzfEFuZCBmcm9tIG5vdyBvbiVdLCBbJXRoaXMgaXMgdGhlfHRoaXMgaXMgYWN0dWFsbHkgdGhlfGhlcmUgaXMgdGhlfHRoaXMgY2FuIGJlIGF8dGhpcyBjYW4gYmUgdGhlJV0gWyVmaXJzdHx2ZXJ5IGZpcnN0fGluaXRpYWx8MXN0fHByaW1hcnklXSBbJWltYWdlfHBpY3R1cmV8Z3JhcGhpY3xpbXByZXNzaW9ufHBob3RvZ3JhcGglXToNCg0KPHNwYW4+eyUgc2V0IGF0dGFjaG1lbnRzID0gZ2V0X2F0dGFjaG1lbnRzKDEsMixwb3N0LklEKSAlfTwvc3Bhbj4NCjxzcGFuPnslIGlmIGF0dGFjaG1lbnRzICV9PC9zcGFuPg0KPHNwYW4+eyUgZm9yIGF0dGFjaG1lbnQgaW4gYXR0YWNobWVudHMgJX08L3NwYW4+DQo8c3Bhbj5bbXdwX2h0bWwgdGFnPVwiaW1nXCIgc3JjPVwie3thdHRhY2htZW50LmltYWdlX3NyY19mdWxsfX1cIiBhbHQ9XCJ7e2F0dGFjaG1lbnQuaW1hZ2VfYWx0fX1cIl0NCnt7YXR0YWNobWVudC5wb3N0X2V4Y2VycHR9fTwvc3Bhbj4NCjxzcGFuPnslIGVuZGZvciAlfTwvc3Bhbj4NCjxzcGFuPnslIGVuZGlmICV9PC9zcGFuPg0KDQpbJUhvdyBhYm91dHxXaGF0IGFib3V0fFRoaW5rIGFib3V0fFdoeSBkb25cJ3QgeW91IGNvbnNpZGVyfFdoeSBub3QgY29uc2lkZXIlXSBbJWltYWdlfHBpY3R1cmV8Z3JhcGhpY3xpbXByZXNzaW9ufHBob3RvZ3JhcGglXSBbJWFib3ZlfG92ZXJ8cHJldmlvdXNseSBtZW50aW9uZWR8ZWFybGllciBtZW50aW9uZWR8cHJlY2VkaW5nJV0/IFslaXN8aXMgYWN0dWFsbHl8d2lsbCBiZXxjYW4gYmV8aXMgdXN1YWxseSVdIFsldGhhdHx3aGljaHxpbiB3aGljaHx0aGF0IHdpbGx8b2Ygd2hpY2glXSBbJWFtYXppbmd8aW5jcmVkaWJsZXxhd2Vzb21lfHJlbWFya2FibGV8d29uZGVyZnVsJV0/Pz8uIFslaWYgeW91IHRoaW5rfGlmIHlvdSBmZWVsfGlmIHlvdSBiZWxpZXZlfGlmIHlvdSB0aGluayBtYXliZXxpZiB5b3VcJ3JlIG1vcmUgZGVkaWNhdGVkJV0gWyVzb3x0aGVyZWZvcmV8dGh1c3xjb25zZXF1ZW50bHl8YW5kIHNvJV0sIElcJ2wgWyVsfG18ZHx0JV0gWyVzaG93IHlvdXxkZW1vbnN0cmF0ZXxleHBsYWluIHRvIHlvdXx0ZWFjaCB5b3V8cHJvdmlkZSB5b3Ugd2l0aCVdIFslc29tZXxhIGZld3xzZXZlcmFsfGEgbnVtYmVyIG9mfG1hbnklXSBbJWltYWdlfHBpY3R1cmV8Z3JhcGhpY3xpbXByZXNzaW9ufHBob3RvZ3JhcGglXSBbJWFnYWlufG9uY2UgYWdhaW58b25jZSBtb3JlfHlldCBhZ2FpbnxhbGwgb3ZlciBhZ2FpbiVdIFslYmVsb3d8YmVuZWF0aHx1bmRlcnxkb3duIGJlbG93fHVuZGVybmVhdGglXToNCjxwIHN0eWxlPVwidGV4dC1hbGlnbjogbGVmdDtcIj48c3Bhbj57JSBzZXQgYXR0YWNobWVudHMgPSBnZXRfYXR0YWNobWVudHMoMywzMCxwb3N0LklEKSAlfTwvc3Bhbj4NCjxzcGFuPnslIGlmIGF0dGFjaG1lbnRzICV9PC9zcGFuPg0KPHNwYW4+eyUgZm9yIGF0dGFjaG1lbnQgaW4gYXR0YWNobWVudHMgJX08L3NwYW4+DQo8c3Bhbj5bbXdwX2h0bWwgdGFnPVwiaW1nXCIgc3JjPVwie3thdHRhY2htZW50LmltYWdlX3NyY19mdWxsfX1cIiBhbHQ9XCJ7e2F0dGFjaG1lbnQuaW1hZ2VfYWx0fX1cIl0NCnt7YXR0YWNobWVudC5wb3N0X2V4Y2VycHR9fTwvc3Bhbj4NCjxzcGFuPnslIGVuZGZvciAlfTwvc3Bhbj4NCjxzcGFuPnslIGVuZGlmICV9PC9zcGFuPjwvcD4NClNvLCBbJWlmIHlvdSB3YW50IHRvfGlmIHlvdSB3aXNoIHRvfGlmIHlvdSB3b3VsZCBsaWtlfGlmIHlvdVwnZCBsaWtlIHRvfGlmIHlvdSBkZXNpcmUgdG98aWYgeW91IGxpa2UgdG8lXSBbJWdldHxyZWNlaXZlfGhhdmV8b2J0YWlufGFjcXVpcmV8c2VjdXJlJV0gWyV0aGVzZXxhbGwgdGhlc2V8YWxsIG9mIHRoZXNlfHRoZSVdIFslYXdlc29tZXxhbWF6aW5nfGdyZWF0fHdvbmRlcmZ1bHxtYWduaWZpY2VudHxmYW50YXN0aWN8aW5jcmVkaWJsZXxvdXRzdGFuZGluZyVdIFslaW1hZ2VzfHBob3Rvc3xwaWNzfHBpY3R1cmVzfGdyYXBoaWNzfHNob3RzJV0gWyVhYm91dHxyZWdhcmRpbmd8cmVsYXRlZCB0byVdICh7e3Bvc3QucG9zdF90aXRsZX19KSwgWyVqdXN0IGNsaWNrfHNpbXBseSBjbGlja3xjbGljayBvbnxwcmVzc3xjbGljayVdIHNhdmUgWyVidXR0b258bGlua3xpY29uJV0gWyV0byBzYXZlfHRvIHN0b3JlfHRvIGRvd25sb2FkJV0gWyV0aGVzZXx0aGUlXSBbJWltYWdlc3xwaG90b3N8cGljc3xwaWN0dXJlc3xncmFwaGljc3xzaG90cyVdIFsldG8geW91cnxmb3IgeW91cnxpbiB5b3VyJV0gWyVjb21wdXRlcnxwY3xwZXJzb25hbCBjb21wdXRlcnxsYXB0b3B8cGVyc29uYWwgcGMlXS4gWyVUaGV5IGFyZXxUaGV5XCdyZXxUaGVzZSBhcmV8VGhlcmVcJ3JlJV0gWyVyZWFkeXxwcmVwYXJlZHxhbGwgc2V0fGF2YWlsYWJsZSVdIGZvciBbJWRvd25sb2FkfG9idGFpbnxkb3duIGxvYWR8c2F2ZXx0cmFuc2ZlciVdLCBbJWlmIHlvdSBsaWtlfGlmIHlvdSB3YW50fGlmIHlvdVwnZCBwcmVmZXJ8aWYgeW91IGxvdmV8aWYgeW91IGFwcHJlY2lhdGV8aWYgeW91XCdkIHJhdGhlciVdIFslYW5kIHdhbnQgdG98YW5kIHdpc2ggdG8lXSBbJWdldCBpdHxoYXZlIGl0fG9idGFpbiBpdHxvd24gaXR8Z3JhYiBpdHx0YWtlIGl0JV0sIFslY2xpY2t8c2ltcGx5IGNsaWNrfGp1c3QgY2xpY2slXSBzYXZlIFslbG9nb3xzeW1ib2x8YmFkZ2UlXSBbJW9uIHRoZXxpbiB0aGUlXSBbJXBhZ2V8d2ViIHBhZ2V8YXJ0aWNsZXxwb3N0JV0sIFslYW5kIGl0IHdpbGx8YW5kIGl0XCdsbCVdIGJlIFslZGlyZWN0bHl8aW1tZWRpYXRlbHl8aW5zdGFudGx5JV0gWyVkb3dubG9hZGVkfGRvd24gbG9hZGVkfHNhdmVkJV0gWyV0byB5b3VyfGluIHlvdXIlXSBbJWNvbXB1dGVyfGxhcHRvcHxwY3xkZXNrdG9wIGNvbXB1dGVyfGxhcHRvcCBjb21wdXRlcnxob21lIGNvbXB1dGVyfG5vdGVib29rIGNvbXB1dGVyJV0uJV0gWyVbJUZpbmFsbHl8TGFzdGx5fEFzIGEgZmluYWwgcG9pbnR8QXQgbGFzdCVdIFslaWYgeW91IHdhbnQgdG98aWYgeW91IGRlc2lyZSB0b3xpZiB5b3UgbGlrZSB0b3xpZiB5b3Ugd2lzaCB0b3xpZiB5b3Ugd291bGQgbGlrZXxpZiB5b3VcJ2QgbGlrZSB0b3xpbiBvcmRlciB0b3xpZiB5b3UgbmVlZCB0byVdIFslZ2V0fHJlY2VpdmV8aGF2ZXxvYnRhaW58c2VjdXJlfGZpbmR8Z3JhYnxnYWluJV0gWyVuZXd8dW5pcXVlJV0gWyVhbmQgdGhlfGFuZCVdIFslbGF0ZXN0fHJlY2VudCVdIFslaW1hZ2V8Z3JhcGhpY3xwaWN0dXJlfHBob3RvJV0gWyVyZWxhdGVkIHdpdGh8cmVsYXRlZCB0byVdICh7e3Bvc3QucG9zdF90aXRsZX19KSwgcGxlYXNlIGZvbGxvdyB1cyBvbiBnb29nbGUgcGx1cyBvciBbJWJvb2ttYXJrfHNhdmV8Ym9vayBtYXJrJV0gWyV0aGlzIHNpdGV8dGhpcyB3ZWJzaXRlfHRoaXMgcGFnZXx0aGlzIGJsb2d8dGhlIHNpdGUlXSwgWyV3ZSB0cnl8d2UgYXR0ZW1wdCVdIG91ciBiZXN0IFsldG8gZ2l2ZSB5b3V8dG8gb2ZmZXIgeW91fHRvIHByb3ZpZGV8dG8gcHJlc2VudCB5b3UlXSBbJWRhaWx5fHJlZ3VsYXIlXSBbJXVwZGF0ZXx1cC1kYXRlfHVwIGdyYWRlJV0gd2l0aCBbJWZyZXNoIGFuZCBuZXd8YWxsIG5ldyBhbmQgZnJlc2glXSBbJWltYWdlc3xncmFwaGljc3xzaG90c3xwaG90b3N8cGljc3xwaWN0dXJlcyVdLiBbJUhvcGUgeW91fFdlIGRvIGhvcGUgeW91JV0gWyVlbmpveXxsb3ZlfGxpa2UlXSBbJXN0YXlpbmd8a2VlcGluZyVdIFslaGVyZXxyaWdodCBoZXJlJV0uIFslRm9yIG1vc3R8Rm9yIG1hbnl8Rm9yIHNvbWUlXSBbJXVwZGF0ZXN8dXAtZGF0ZXN8dXBncmFkZXMlXSBhbmQgWyVsYXRlc3R8cmVjZW50JV0gWyVuZXdzfGluZm9ybWF0aW9uJV0gYWJvdXQgKHt7cG9zdC5wb3N0X3RpdGxlfX0pIFslaW1hZ2VzfGdyYXBoaWNzfHNob3RzfHBob3Rvc3xwaWNzfHBpY3R1cmVzJV0sIHBsZWFzZSBraW5kbHkgZm9sbG93IHVzIG9uIFsldHdpdHRlcnx0d2VldHMlXSwgcGF0aCwgSW5zdGFncmFtIGFuZCBnb29nbGUgcGx1cywgb3IgeW91IG1hcmsgdGhpcyBwYWdlIG9uIFslYm9va21hcmt8Ym9vayBtYXJrJV0gWyVzZWN0aW9ufGFyZWElXSwgWyVXZSB0cnl8V2UgYXR0ZW1wdCVdIFsldG8gZ2l2ZSB5b3V8dG8gb2ZmZXIgeW91fHRvIHByZXNlbnQgeW91fHRvIHByb3ZpZGUgeW91IHdpdGglXSBbJXVwZGF0ZXx1cCBncmFkZXx1cC1kYXRlJV0gWyVwZXJpb2RpY2FsbHl8cmVndWxhcmx5JV0gd2l0aCBbJWZyZXNoIGFuZCBuZXd8YWxsIG5ldyBhbmQgZnJlc2glXSBbJWltYWdlc3xncmFwaGljc3xzaG90c3xwaG90b3N8cGljc3xwaWN0dXJlcyVdLCBbJWVuam95fGxpa2V8bG92ZSVdIHlvdXIgWyVicm93c2luZ3xzZWFyY2hpbmd8ZXhwbG9yaW5nfHN1cmZpbmclXSwgYW5kIGZpbmQgWyV0aGUgYmVzdHx0aGUgaWRlYWx8dGhlIHBlcmZlY3R8dGhlIHJpZ2h0JV0gZm9yIHlvdS4lXQ0KDQo8c3Bhbj5bJVRoYW5rcyBmb3IgdmlzaXRpbmd8SGVyZSB5b3UgYXJlIGF0JV0gWyVvdXIgc2l0ZXxvdXIgd2Vic2l0ZSVdLCBbJWFydGljbGV8Y29udGVudCVdYWJvdmXCoCg8ZW0+e3twb3N0LnBvc3RfdGl0bGV9fTwvZW0+KSBwdWJsaXNoZWQgYnkge3twb3N0LmF1dGhvcl9uYW1lfX0gYXQge3twb3N0LmRhdGV9fS4gWyVUb2RheXxOb3dhZGF5c3xBdCB0aGlzIHRpbWUlXSBbJXdlIGFyZXx3ZVwncmUlXSBbJWRlbGlnaHRlZHxwbGVhc2VkfGV4Y2l0ZWQlXSB0byBbJWFubm91bmNlfGRlY2xhcmUlXSBbJXRoYXQgd2V8d2UlXSBbJWhhdmUgZm91bmR8aGF2ZSBkaXNjb3ZlcmVkJV0gWyVhIHZlcnl8YW4gZXh0cmVtZWx5fGFuIGluY3JlZGlibHl8YW4gYXdmdWxseSVdaW50ZXJlc3RpbmcgWyV0b3BpY3xuaWNoZXxjb250ZW50JV10byBiZSBbJWRpc2N1c3NlZHxyZXZpZXdlZHxwb2ludGVkIG91dCVdLCBbJW5hbWVseXx0aGF0IGlzJV0gKDxlbT57e3Bvc3QucG9zdF90aXRsZX19PC9lbT4pIFslTWFueSBwZW9wbGV8TG90cyBvZiBwZW9wbGV8TWFueSBpbmRpdmlkdWFsc3xTb21lIHBlb3BsZXxNb3N0IHBlb3BsZSVdIFslbG9va2luZyBmb3J8c2VhcmNoaW5nIGZvcnx0cnlpbmcgdG8gZmluZHxhdHRlbXB0aW5nIHRvIGZpbmQlXSBbJWluZm9ybWF0aW9uIGFib3V0fGRldGFpbHMgYWJvdXR8c3BlY2lmaWNzIG9mfGluZm8gYWJvdXQlXSg8ZW0+e3twb3N0LnBvc3RfdGl0bGV9fTwvZW0+KSBbJWFuZCBkZWZpbml0ZWx5fGFuZCBjZXJ0YWlubHl8YW5kIG9mIGNvdXJzZSVdIFslb25lIG9mIHRoZW18b25lIG9mIHRoZXNlJV0gaXMgeW91LCBpcyBub3QgaXQ/PC9zcGFuPiI7fWk6NDthOjQ6e3M6NDoibmFtZSI7czoxOToiQXRpZ2FuIEF0dGFjaG1lbnQgNCI7czo0OiJ0eXBlIjtzOjEwOiJhdHRhY2htZW50IjtzOjQ6Im5vdGUiO3M6MTk6IkF0aWdhbiBBdHRhY2htZW50IDQiO3M6NzoiY29udGVudCI7czoyOTQzOiJbJUZyb20gdGhlfFRocm91Z2ggdGhlJV0gWyV0aG91c2FuZHx0aG91c2FuZHMgb2YlXSBbJWltYWdlc3xwaWN0dXJlc3xwaG90b3N8cGhvdG9ncmFwaHMlXSBbJW9uIHRoZSBpbnRlcm5ldHxvbiB0aGUgd2VifG9uIHRoZSBuZXR8b25saW5lfG9uLWxpbmUlXSBbJWFib3V0fHJlZ2FyZGluZ3xjb25jZXJuaW5nfHdpdGggcmVnYXJkcyB0b3xpbiByZWxhdGlvbiB0byVdIDxzdHJvbmc+e3trZXl3b3JkfX08L3N0cm9uZz4sIFsld2V8d2UgYWxsfCVdIFslcGlja3N8Y2hvaWNlc3xzZWxlY3RzJV0gWyV0aGUgYmVzdHx0aGUgdmVyeSBiZXN0fHRoZSB0b3AlXSBbJWNvbGxlY3Rpb25zfHNlbGVjdGlvbnN8Y2hvaWNlc3xzZXJpZXN8bGlicmFyaWVzJV0gWyV3aXRofGFsb25nIHdpdGh8dG9nZXRoZXIgd2l0aHx1c2luZ3xoYXZpbmclXSBbJWJlc3R8Z3JlYXRlc3R8aWRlYWwlXSBbJXJlc29sdXRpb258cXVhbGl0eXxpbWFnZSByZXNvbHV0aW9uJV0gWyVqdXN0IGZvcnxzaW1wbHkgZm9yfG9ubHkgZm9yfGV4Y2x1c2l2ZWx5IGZvciVdIFsleW91fHlvdSBhbGwlXSwgWyVhbmQgdGhpc3xhbmQgbm93IHRoaXMlXSBbJWltYWdlc3xwaWN0dXJlc3xwaG90b3N8cGhvdG9ncmFwaHMlXSBbJWlzfGlzIGFjdHVhbGx5fGlzIHVzdWFsbHklXSBbJW9uZSBvZnxhbW9uZ3xjb25zaWRlcmVkIG9uZSBvZnxvbmUgYW1vbmclXSBbJWltYWdlc3xwaWN0dXJlc3xwaG90b3N8cGhvdG9ncmFwaHN8Z3JhcGhpY3MlXSBbJWNvbGxlY3Rpb25zfHNlbGVjdGlvbnN8Y2hvaWNlc3xzZXJpZXN8bGlicmFyaWVzJV0gWyVpbiBvdXJ8d2l0aGluIG91cnxpbnNpZGUgb3VyfGluIHlvdXJ8aW4gdGhpcyVdIFslYmVzdHxncmVhdGVzdHxmaW5lc3R8aWRlYWx8dmVyeSBiZXN0JV0gWyVpbWFnZXN8cGljdHVyZXN8cGhvdG9zfHBob3RvZ3JhcGhzfGdyYXBoaWNzJV0gZ2FsbGVyeSBbJWFib3V0fHJlZ2FyZGluZ3xjb25jZXJuaW5nfHdpdGggcmVnYXJkcyB0b3xpbiByZWxhdGlvbiB0byVdIDxlbT57e3Bvc3QucG9zdF90aXRsZX19PC9lbT4uIFslSSBob3BlfEkgcmVhbGx5IGhvcGV8SSBhbSBob3Bpbmd8SSdtIGhvcGluZ3xMZXRzIGhvcGUlXSBbJXlvdSB3aWxsfHlvdSdsbHx5b3UgbWF5fHlvdSBjYW58eW91IG1pZ2h0JV0gWyVsaWtlIGl0fGVuam95IGl0fHdhbnQgaXR8YXMgaXR8dGhpbmsgaXQncyBncmVhdCVdLg0KDQpbJVRoaXN8VGhpcyBwYXJ0aWN1bGFyfFRoaXMga2luZCBvZnxUaGlzIHNwZWNpZmljfFRoYXQlXSBbJWltYWdlfHBpY3R1cmV8Z3JhcGhpY3xpbXByZXNzaW9ufHBob3RvZ3JhcGglXSAoPGVtPnt7YXR0YWNobWVudC5wb3N0X3RpdGxlfX08L2VtPikgWyVhYm92ZXxvdmVyfHByZXZpb3VzbHkgbWVudGlvbmVkfGVhcmxpZXIgbWVudGlvbmVkfHByZWNlZGluZyVdIFslaXN8aXMgYWN0dWFsbHl8d2lsbCBiZXxjYW4gYmV8aXMgdXN1YWxseSVdIFslbGFiZWxsZWR8YnJhbmRlZHxjbGFzc2VkJV0gWyV3aXRofGFsb25nIHdpdGh8dG9nZXRoZXIgd2l0aHx1c2luZ3xoYXZpbmclXTogeyUgc2V0IHRhZ3MgPSBnZXRfdGFncygxLDMscG9zdC5JRCkgJX17JSBpZiB0YWdzICV9eyUgZm9yIHRhZyBpbiB0YWdzICV9e3t0YWcubmFtZX19LCB7JSBlbmRmb3IgJX0ueyUgZW5kaWYgJX0NCg0KWyVwb3N0ZWR8c3VibWl0dGVkfHB1Ymxpc2hlZHxwdXQgdXB8cGxhY2VkJV0gWyVieXx0aHJvdWdofHNpbXBseSBieXxieSBzaW1wbHl8YnkgbWVhbnMgb2YlXSB7e3Bvc3QuYXV0aG9yX25hbWV9fSBbJWF0fGZyb218d2l0aHxpbnxvbiVdIHt7cG9zdC5kYXRlfX0uIFslVG8gc2VlfFRvIGRldGVybWluZXxUbyBmaW5kIG91dHxUbyB2aWV3fFRvIGRpc2NvdmVyJV0gWyVhbGx8anVzdCBhYm91dCBhbGx8YWxtb3N0IGFsbHxtb3N0fG1hbnklXSBbJWltYWdlc3xwaWN0dXJlc3xwaG90b3N8cGhvdG9ncmFwaHN8Z3JhcGhpY3MlXSBbJWlufHdpdGhpbnxpbnNpZGV8dGhyb3VnaG91dHx3aXRoJV0ge3twb3N0LnBvc3RfdGl0bGV9fSBbJWltYWdlc3xwaWN0dXJlc3xwaG90b3N8cGhvdG9ncmFwaHN8Z3JhcGhpY3MlXSBnYWxsZXJ5IFslcGxlYXNlfG1ha2Ugc3VyZSB5b3V8eW91IHNob3VsZHxyZW1lbWJlciB0b3x5b3UgbmVlZCB0byVdIFslZm9sbG93fGFkaGVyZSB0b3xzdGljayB0b3xjb21wbHkgd2l0aHxhYmlkZSBieSVdIDxhIGhyZWY9Int7cG9zdC51cmx9fSIgdGl0bGU9Int7cG9zdC5wb3N0X3RpdGxlfX0iPlsldGhpc3x0aGlzIHBhcnRpY3VsYXJ8dGhpcyBraW5kIG9mfHRoaXMgc3BlY2lmaWN8dGhhdCVdIFslbGlua3xoeXBlcmxpbmt8d2Vic2l0ZSBsaW5rfHVybHx3ZWIgcGFnZSBsaW5rJV08L2E+Lg0KDQpbJVslVGhlIE1vc3QgfFRoZSB8ICVdWyVFbGVnYW50fFN0eWxpc2h8QXdlc29tZXxBbWF6aW5nfEJyaWxsaWFudHxJbmNyZWRpYmxlJV0gWyVbJWFuZHxhcyB3ZWxsIGFzfGFuZCBhbHNvfGFsb25nIHdpdGh8aW4gYWRkaXRpb24gdG8lXSBbJUF0dHJhY3RpdmV8QmVhdXRpZnVsfFN0dW5uaW5nfEdvcmdlb3VzfExvdmVseXxJbnRlcmVzdGluZyVdIHwgJV18ICVde3trZXl3b3JkfHRpdGxlfX0gWyVmb3J8d2l0aCByZWdhcmQgdG98cmVnYXJkaW5nfHBlcnRhaW5pbmcgdG98aW50ZW5kZWQgZm9yJV0gWyVbJUluc3BpcmV8RW5jb3VyYWdlfE1vdGl2YXRlfEludmlnb3JhdGV8UmVhbGx5IGVuY291cmFnZSVdfFslWW91ciBob21lfFlvdXIgaG91c2V8WW91ciBwcm9wZXJ0eXxZb3VyIG93biBob21lfFRoZSBob3VzZSVdfFslWyVQcmVzZW50fEV4aXN0aW5nfEN1cnJlbnR8Rm91bmR8UHJvdmlkZSVdfCVdIFslSG9tZXxIb3VzZXxSZXNpZGVuY2V8SG91c2Vob2xkfFByb3BlcnR5JV18WyVbJUNvenl8Q29tZm9ydGFibGV8Q29tZnl8V2FybXxJbnZpdGluZyVdfFslRHJlYW18RGVzaXJlfEZhbnRhc3l8QXNwaXJhdGlvbnxXaXNoJV0lXXxbJUhvbWV8SG91c2V8UmVzaWRlbmNlfEhvdXNlaG9sZHxQcm9wZXJ0eSVdJV0iO31pOjU7YTo0OntzOjQ6Im5hbWUiO3M6MTY6IkF0aWdhbiBDYXB0aW9uIDQiO3M6NDoidHlwZSI7czoxMzoiaW1hZ2VfY2FwdGlvbiI7czo0OiJub3RlIjtzOjE2OiJBdGlnYW4gQ2FwdGlvbiA0IjtzOjc6ImNvbnRlbnQiO3M6NDk6Int7YXR0YWNobWVudC5wb3N0X3RpdGxlfX0gYnkge3twb3N0LmF1dGhvcl9uYW1lfX0iO319</textarea>
                    </div>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="newTemplate" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">New Template</h4>
            </div>

            <div class="modal-body">
              
              <div>
              <div class="form-group">
                <label class="control-label col-md-3" for="template-name">Name</label>
                <div class="col-md-9">
                  <input type="text" id="template-name" class="form-control" required="">
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="template-type">Template Type</label>
                <div class="col-md-9">
                  <select id="template-type" class="form-control">
                    <option value="post">Post</option>
                    <option value="attachment">Attachment</option>
                    <option value="image_caption">Image Caption</option>
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-3" for="template-note">Note</label>
                <div class="col-md-9">
                  <input type="text" id="template-note" class="form-control" required="">
                </div>
              </div>
            </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                 Create
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="updateTemplate" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Template ()</h4>
            </div>

            <div class="modal-body">
              
              <div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="update-template-name">Name</label>
                  <div class="col-md-9">
                    <input type="text" id="update-template-name" class="form-control" required="">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3" for="update-template-type">Template Type</label>
                  <div class="col-md-9">
                    <select id="update-template-type" class="form-control">
                      <option value="post">Post</option>
                      <option value="attachment">Attachment</option>
                      <option value="image_caption">Image Caption</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3" for="update-template-note">Note</label>
                  <div class="col-md-9">
                    <input type="text" id="update-template-note" class="form-control" required="">
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                 Update
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="deleteTemplate" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Are you sure want to delete this template?</h4>
            </div>

            <div class="modal-body">
              
              <div>
                <div class="form-group">
                  <label class="control-label col-md-3" for="update-template-name">Name</label>
                  <div class="col-md-9">
                    <p class="form-control-static"></p>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3" for="update-template-type">Template Type</label>
                  <div class="col-md-9">
                    <p class="form-control-static"></p>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3" for="update-template-note">Note</label>
                  <div class="col-md-9">
                    <p class="form-control-static"></p>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-danger">
                 Delete
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="importCode" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Import Templates</h4>
            </div>
            <div class="modal-body">
              
              <div class="form-group">
                <label class="control-label col-md-12" for="import-code">Put your backup code bellow:</label>
                <div class="col-md-12">
                  <textarea rows="10" class="form-control" required="" id="import-code"></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                 Import
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div id="importDefault" class="modal fade" data-backdrop="static" role="dialog" style="overflow-y:visible;">
      <div class="modal-dialog">
        <div class="modal-content">
          <form action="" method="post" class="form-horizontal form-bordered">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">×</button>
              <h4 class="modal-title">Import Default Templates</h4>
            </div>
            <div class="modal-body">
              
              <div class="form-group">
                <div class="col-md-12">
                  <h4>Are you sure?</h4>
                  <p>By clicking Import button, you will importing default templates.</p>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">
                 Import
              </button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>