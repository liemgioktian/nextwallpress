<?php 
/*
Plugin Name: Next WallPress
URI: http://www.000software.com
Description: Plugin Next WallPress
Author: 000software 
Version: 1.2
Author URI: http://www.000software.com
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.txt
*/

define('NEXT_WALLPRESS_PATH',  plugin_dir_path( __FILE__ ));
define('NEXT_WALLPRESS_URL', plugin_dir_url(__FILE__));

include NEXT_WALLPRESS_PATH . 'core.php';