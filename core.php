<?php

add_action('admin_menu', function () {

  add_menu_page('Next WallPress', 'Next WallPress', 'manage_options', 'next-wallpress', 
    function () {
      wp_enqueue_style( 'nextwallpress', NEXT_WALLPRESS_URL . 'asset/bootstrap.min.css');
      include NEXT_WALLPRESS_PATH . 'views/campaign.php';
    }
  , 'dashicons-format-gallery');

  add_submenu_page('next-wallpress', 'Autoblog', 'Autoblog', 'manage_options', 'next-wallpress-autoblog', function () {
    wp_enqueue_style( 'nextwallpress', NEXT_WALLPRESS_URL . 'asset/bootstrap.min.css');
    include NEXT_WALLPRESS_PATH . 'views/autoblog.php';
  });

  add_submenu_page('next-wallpress', 'Bulk Poster', 'Bulk Poster', 'manage_options', 'next-wallpress-poster', function () {
    wp_enqueue_style( 'nextwallpress', NEXT_WALLPRESS_URL . 'asset/bootstrap.min.css');
    include NEXT_WALLPRESS_PATH . 'views/bulk-poster.php';
  });

  add_submenu_page('next-wallpress', 'Campaign', 'Campaign', 'manage_options', 'edit.php?post_type=wallpress_campaign', null);

  add_submenu_page('next-wallpress', 'Post Management', 'Post Management', 'manage_options', 'next-wallpress-posts', function () {
    wp_enqueue_style( 'nextwallpress', NEXT_WALLPRESS_URL . 'asset/bootstrap.min.css');
    include NEXT_WALLPRESS_PATH . 'views/posts.php';
  });

  add_submenu_page('next-wallpress', 'Settings', 'Settings', 'manage_options', 'next-wallpress-settings', function () {
    wp_enqueue_style( 'nextwallpress', NEXT_WALLPRESS_URL . 'asset/bootstrap.min.css');
    include NEXT_WALLPRESS_PATH . 'views/settings.php';
  });

  add_submenu_page('next-wallpress', 'Templates', 'Templates', 'manage_options', 'next-wallpress-templates', function () {
    wp_enqueue_style( 'nextwallpress', NEXT_WALLPRESS_URL . 'asset/bootstrap.min.css');
    include NEXT_WALLPRESS_PATH . 'views/templates.php';
  });

  add_submenu_page('next-wallpress', 'Asik asik', 'Asik asik', 'manage_options', 'next-wallpress-asikasik', function () {
    if (!class_exists( 'WP_Http' )) include_once( ABSPATH . WPINC . '/class-http.php' );
    require_once (NEXT_WALLPRESS_PATH . 'lib/simplehtmldom/simple_html_dom.php');

    $http     = new WP_Http();
    $parser   = new simple_html_dom();
    $userAgent= 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36';
    $url      = 'https://www.google.com/search?q=ayla+type+m&client=firefox-b-ab&source=lnms&tbm=isch&sa=X&biw=1280&bih=721';
    $response = $http->request($url, array('user-agent' => $userAgent));
    $parser->load($response['body'], true, false);
    $limit = 5;
    foreach ($parser->find('div[class^="rg_meta"]') as $meta) {
      if ($limit <= 0) continue;
      $obj = json_decode($meta->innertext);
      nextwallpress_insert_attachment_from_url ($obj->ou);
      $limit--;
    }
    die('done');
  });

});

add_action( 'init', function () {

  register_post_type( 'wallpress_campaign',
    array(
      'labels'          => array(
        'name'          => __( 'Campaigns' ),
        'singular_name' => __( 'Campaign' )
      ),
      'public'          => true,
      'has_archive'     => true,
      'show_in_menu'    => false,
      'supports' => array(
        'title', 
      )
    )
  );

});

function nextwallpress_insertPosts () {
  $ids = array();
  for ($p = 0; $p <= 3; $p++) $ids[] = wp_insert_post(array(
    'post_title'  => 'Create Campaign ' . $p,
    'post_type'   => 'wallpress_campaign'
  ));
  die(var_dump($ids));
}

function nextwallpress_insert_attachment_from_url ($url, $post_id = null) {
  if (!class_exists( 'WP_Http' )) include_once( ABSPATH . WPINC . '/class-http.php' );
  $http     = new WP_Http();
  $response = $http->request($url);
  if( $response['response']['code'] != 200 ) return false;

  $upload   = wp_upload_bits( basename($url), null, $response['body'] );
  if( !empty( $upload['error'] ) ) return false;

  $file_path= $upload['file'];
  $file_name= basename( $file_path );
  $file_type= wp_check_filetype( $file_name, null );
  $attachment_title = sanitize_file_name( pathinfo( $file_name, PATHINFO_FILENAME ) );
  $wp_upload_dir = wp_upload_dir();

  $post_info = array(
    'guid'          => $wp_upload_dir['url'] . '/' . $file_name, 
    'post_mime_type'=> $file_type['type'],
    'post_title'    => $attachment_title,
    'post_content'  => '',
    'post_status'   => 'inherit',
  );

  $attach_id = wp_insert_attachment( $post_info, $file_path, $post_id );
  require_once( ABSPATH . 'wp-admin/includes/image.php' );
  $attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );
  wp_update_attachment_metadata( $attach_id,  $attach_data );

  return $attach_id;
}